import sbt._

object Version {
  lazy val pekkoVersion = "1.0.1"
  lazy val playJson        = "2.10.4"
}

object Library {
  val pekkohttp            = "org.apache.pekko"           %% "pekko-http"                   % Version.pekkoVersion
  val pekkostream          = "org.apache.pekko"           %% "pekko-stream"                 % Version.pekkoVersion
  val playjson            = "com.typesafe.play"           %% "play-json"                   % Version.playJson
  val logback             = "ch.qos.logback"              %  "logback-classic"             % "1.5.6"
  val scalalogging        = "com.typesafe.scala-logging"  %% "scala-logging"               % "3.9.5"
  val pekkohttptestkit     = "org.apache.pekko"           %% "pekko-http-testkit"           % Version.pekkoVersion
  val pekkotestkit         = "org.apache.pekko"           %% "pekko-testkit"                % Version.pekkoVersion
  val pekkostreamtestkit   = "org.apache.pekko"           %% "pekko-stream-testkit"         % Version.pekkoVersion
  val scalatest           = "org.scalatest"               %% "scalatest"                   % "3.2.9"
  val mockitocore         = "org.mockito"                 % "mockito-core"                 % "5.10.0"
  val anorm               = "org.playframework.anorm"     %% "anorm"                       % "2.7.0"
  val postgresql          = "org.postgresql"              %  "postgresql"                  % "9.4.1212"
  val hikaricp            = "com.zaxxer"                  %  "HikariCP"                    % "3.4.5"
  val h2                  = "com.h2database"              % "h2"                           % "1.4.200"
  val scalatestplay       = "org.scalatestplus.play"      %% "scalatestplus-play"          % "7.0.1"

}

object Dependencies {

  import Library._

  val scalaVersion = "3.3.1"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/")
  )

  val runDep = run(
    pekkohttp,
    pekkostream,
    playjson,
    logback,
    scalalogging,
    anorm,
    postgresql,
    hikaricp
  )

  val testDep = test(
    pekkohttptestkit,
    pekkotestkit,
    pekkostreamtestkit,
    scalatest,
    mockitocore,
    hikaricp,
    h2,
    scalatestplay
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % Test)

}
