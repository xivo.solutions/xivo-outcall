package app

import org.apache.pekko.actor.ActorSystem
import anorm.SqlParser.*
import anorm.{SQL, SqlQuery, as, execute}
import com.zaxxer.hikari.HikariConfig
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar.mock
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.wordspec.AnyWordSpecLike

import java.sql.{Connection, SQLException}
import scala.concurrent.duration.{FiniteDuration, MILLISECONDS}
import scala.util.{Failure, Success}

class DatabaseConnectionPoolSpec extends AnyWordSpecLike with Matchers with BeforeAndAfterEach with ScalaFutures {

  override implicit def patienceConfig: PatienceConfig = PatienceConfig(timeout = Span(500, Millis), interval = Span(5, Millis))
  implicit val system: ActorSystem = ActorSystem()

  var pool: DatabaseConnectionPool = _

  class Helper {
    val configuration: Configuration = mock[Configuration]

    when(configuration.DB_CNX_TIMEOUT).thenReturn(FiniteDuration(500, MILLISECONDS))

    pool = new DatabaseConnectionPool(configuration) {
      override def setHikariConfiguration(config: HikariConfig): Unit = {
        config.setJdbcUrl("jdbc:h2:mem:testdb")
        config.setDriverClassName("org.h2.Driver")
      }
    }
  }

  override protected def afterEach(): Unit = pool.closeConnectionPool

  "DatabaseConnectionPool" should {
    "initialize the pool" in new Helper {
      pool.dataSource.isRunning shouldBe true
    }

    "execute sql query and close connection" in new Helper {
      import concurrent.ExecutionContext.Implicits.global
      var conUnderTest: Connection = _
      val query: SqlQuery = SQL("""SELECT 1""".stripMargin)
      val res = pool.withConnection { implicit c =>
        query.execute()
        c
      }

      res.onComplete {
        case Success(c) => c.isClosed shouldEqual true
        case Failure(t) => fail("Request did not end")
      }
    }

    "not execute a sql query with closed pool" in new Helper {
      pool.closeConnectionPool
      val query = SQL("""SELECT 1""".stripMargin)
      val res = pool.withConnection { implicit c => query.execute() }

      res.failed.futureValue shouldBe a[SQLException]
    }

    "close connection on failed sql query" in new Helper {
      val query = SQL("""SELECT nocolumn FROM notable""".stripMargin)
      var conUnderTest: Connection = _

      val res = pool.withConnection { implicit c =>
        conUnderTest = c
        query.as(scalar[String].single)
      }

      res.failed.futureValue shouldBe a[SQLException]
      conUnderTest.isClosed shouldBe true
    }

    "close the pool" in new Helper {
      pool.closeConnectionPool
      pool.dataSource.isClosed shouldBe true
    }
  }
}
