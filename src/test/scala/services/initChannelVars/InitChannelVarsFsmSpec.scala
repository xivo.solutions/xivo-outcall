package services.initChannelVars

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{TestFSMRef, TestProbe}
import app.Configuration.ServiceId
import ari.actions.{Ari, Channels}
import models.ChannelVariables.*
import models.*
import models.events.bus.ContinueInDialplan
import models.events.ws.{ChannelVarset, StasisStart}
import org.mockito.ArgumentMatchers.{any, anyLong, anyString}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus.*
import services.FsmFactory.{Initial, Uninitialized}
import services.initChannelVars.InitChannelVarsFsm.*
import services.rights.RightsDbActions.User
import services.rights.RightsDbManager.ResponseUser
import services.rights.{RightsDbActions, RightsHelper}
import services.routing.RoutingDbActions.Route
import services.routing.RoutingDbManager.ResponseRoute
import services.routing.RoutingHelper
import services.{AriEventBus, DatabaseManagers}
import org.mockito.Mockito.{doNothing, reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.ExecutionContext

class InitChannelVarsFsmSpec extends AnyWordSpecLike with Matchers with MockitoSugar with Eventually {

  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = system.dispatcher

  val aribus = mock[AriEventBus]

  val testprobe = TestProbe()

  val routingDbManager = TestProbe()
  val routingHelper = new RoutingHelper(aribus)

  val rightsDbManager = TestProbe()
  val rightsDbActions = mock[RightsDbActions]
  val rightsHelper = new RightsHelper(rightsDbActions)

  val initChannelDbManager = TestProbe()
  val initChannelHelper = new InitChannelVarsHelper(aribus)
  val initChannelHelperMock = mock[InitChannelVarsHelper]

  val dbManagers = mock[DatabaseManagers]
  when(dbManagers.routeManager).thenReturn(routingDbManager.ref)
  when(dbManagers.rightsManager).thenReturn(rightsDbManager.ref)
  when(dbManagers.initChannelVarsManager).thenReturn(initChannelDbManager.ref)

  val ari = new Ari(aribus, InitChannelVarsApp)

  val caller = Caller(Some("User One"), Some("1001"))
  val connected = Connected(Some(""), Some(""))
  val dialplan = Dialplan("default", "9999", 3)
  val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

  val stasisStart = StasisStart(List(), channel, "08:00:27:80:c1:5f", "schedules")

  val fsm = TestFSMRef(new InitChannelVarsFsm(aribus, ari, dbManagers,
    routingHelper, rightsHelper, initChannelHelper, channel))

  class Helper() {
    val ariBusMock = mock[AriEventBus]
    //val rightsHelper = mock[RightsHelper]
    val ari = mock[Ari]
    val channels = mock[Channels]
    when(ari.channels).thenReturn(channels)
    val fsmWithMock = TestFSMRef(new InitChannelVarsFsm(ariBusMock, ari, dbManagers,
      routingHelper, rightsHelper, initChannelHelperMock, channel))

    val channelData = ChannelData(Map(CALLFORWARDED -> None, FORWARDERNAME -> None, USERID -> Some("1"),
      XIVO_FWD_ACTIONARG1 -> None, SOURCE_NUMBER -> Some("2031"), MDS_NAME -> Some("default"),
      DESTINATION_NUMBER -> Some("0123456"), FWD_REFERER -> None, XIVO_FWD_ACTION -> None,
      XIVO_BASE_CONTEXT -> Some("default")))
  }

  "InitChannelVarsFsm" should {
    "start in Initial state" in {
      assert(fsm.stateName == Initial)
      assert(fsm.stateData == Uninitialized)
    }

    "transition to AwaitChannelVars state" in {
      fsm ! stasisStart

      val channelData = ChannelData(Map("MDS_NAME" -> Some("default")))
      val fsmCtx = InitFsmContext(List(DESTINATION_NUMBER, XIVO_BASE_CONTEXT, USERID, CALLFORWARDED, FORWARDERNAME,
        FWD_REFERER, XIVO_FWD_ACTION, XIVO_FWD_ACTIONARG1, SOURCE_NUMBER), channel, channelData)

      assert(fsm.stateName == AwaitChannelVars)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitRoute state" in {
      val responses = List(
        ChannelVarResponse(DESTINATION_NUMBER, new ChannelVar(Some("0123456"))),
        ChannelVarResponse(XIVO_BASE_CONTEXT, new ChannelVar(Some("default"))),
        ChannelVarResponse(USERID, new ChannelVar(Some("1"))),
        ChannelVarResponse(CALLFORWARDED, new ChannelVar(None)),
        ChannelVarResponse(FORWARDERNAME, new ChannelVar(None)),
        ChannelVarResponse(FWD_REFERER, new ChannelVar(None)),
        ChannelVarResponse(XIVO_FWD_ACTION, new ChannelVar(None)),
        ChannelVarResponse(XIVO_FWD_ACTIONARG1, new ChannelVar(None)),
        ChannelVarResponse(SOURCE_NUMBER, new ChannelVar(Some("2031")))
      )

      val someVarSetEvent = ChannelVarset("XIVO_SIPCALLID", "f1cfe4a-59b51d19-11ce76d8@10.0.1.11", channel, "asterisk-1", "app1")

      responses.foreach(r => fsm ! r)
      fsm ! someVarSetEvent


      val channelData = ChannelData(responses.map(r => r.channelVarName -> r.channelVarValue.`value`).toMap)
        .addChannelVar("MDS_NAME", Some("default"))

      val fsmCtx = InitFsmContext(List(), channel, channelData)

      routingDbManager.expectMsg(GetRoute(fsmCtx.channelData))

      assert(fsm.stateName == AwaitRoute)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitSetRouteInDialplan state" in {
      val response = ResponseRoute(List(Route(1, None, None, None, Some("0123"), false)))

      fsm ! response

      val channelData = ChannelData(Map(CALLFORWARDED -> None, FORWARDERNAME -> None, USERID -> Some("1"),
        XIVO_FWD_ACTIONARG1 -> None, SOURCE_NUMBER -> Some("2031"), MDS_NAME -> Some("default"),
        DESTINATION_NUMBER -> Some("0123456"), FWD_REFERER -> None, XIVO_FWD_ACTION -> None,
        XIVO_BASE_CONTEXT -> Some("default")))

      val fsmCtx = InitFsmContext(List("XIVO_ROUTE_ID", "XIVO_OUTCALLPREPROCESS_SUBROUTINE"), channel, channelData, Some("0123"))

      assert(fsm.stateName == AwaitSetRouteInDialplan)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitUser state" in {
      val response1 = ChannelVarset("XIVO_ROUTE_ID", "1", channel, "asterisk-123", "initChannelVars")
      val response2 = ChannelVarset("XIVO_OUTCALLPREPROCESS_SUBROUTINE", "", channel, "asterisk-123", "initChannelVars")

      fsm ! response1
      fsm ! response2

      val channelData = ChannelData(Map(CALLFORWARDED -> None, FORWARDERNAME -> None, USERID -> Some("1"),
        XIVO_FWD_ACTIONARG1 -> None, SOURCE_NUMBER -> Some("2031"), MDS_NAME -> Some("default"),
        DESTINATION_NUMBER -> Some("0123456"), FWD_REFERER -> None, XIVO_FWD_ACTION -> None,
        XIVO_BASE_CONTEXT -> Some("default")))

      val fsmCtx = InitFsmContext(List(), channel, channelData, Some("0123"))

      rightsDbManager.expectMsg(GetUser(1))

      assert(fsm.stateName == AwaitUser)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "set recording filename, call options and xds user id and go to AwaitSetCallSettings" in new Helper() {
      val response = ResponseUser(Some(User(1, None, true, "test", false, false)))

      when(initChannelHelperMock.setCallerId(any(), any(), any(), any()))
        .thenReturn(List())

      when(initChannelHelperMock.setUserRecordingFilename(any(), any(), anyLong()))
        .thenReturn(List(SetUserRecordingFilename(CALL_RECORD_FILE_NAME)))

      when(initChannelHelperMock.setXdsUserId(any(), anyString()))
        .thenReturn(List(SetXdsUserId(XIVO_XDS_USERID)))

      when(initChannelHelperMock.setCallOptionVars(any(), any(), any(), any()))
        .thenReturn(List())

      fsmWithMock.setState(AwaitUser, InitFsmContext(List(), channel, channelData, Some("0123")))

      fsmWithMock ! response

      val expectedNewCtx = InitFsmContext(List("XIVO_CALLRECORDFILE", "XIVO_XDS_USERID"), channel, channelData, Some("0123"))

      verify(initChannelHelperMock).setCallerId(channel, Some("test"), Some("0123"), false)
      verify(initChannelHelperMock).setUserRecordingFilename(any(), any(), anyLong())
      verify(initChannelHelperMock).setXdsUserId(channel, "1")

      assert(fsmWithMock.stateName == AwaitSetCallSettings)
      assert(fsmWithMock.stateData == expectedNewCtx)
      assert(fsmWithMock.isStateTimerActive == true)
    }

    "set caller id from route if user is not found" in new Helper() {
      testprobe watch fsmWithMock
      fsmWithMock.setState(AwaitSetRouteInDialplan, InitFsmContext(List(), channel, ChannelData(Map())))

      val response = ChannelVarset("XIVO_ROUTE_ID", "129", channel, "asterisk-123", "initChannelVars")

      fsmWithMock ! response

      val action = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, None, None, None), InitChannelVarsApp, channel.id)
      )

      verify(initChannelHelperMock).setCallerId(channel, None, None, false)
      verify(channels).continue(channel.id, None, None, None, None, channel.id)
      testprobe.expectTerminated(fsmWithMock)
    }

    "continue in dialplan and stop" in new Helper() {
      testprobe watch fsmWithMock
      fsmWithMock.setState(AwaitSetCallSettings, InitFsmContext(List(), channel, ChannelData(Map())))

      val response = ChannelVarset("XIVO_CALLRECORDFILE", "user-2031.wav", channel, "asterisk-123", "initChannelVars")

      fsmWithMock ! response

      val action = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, None, None, None), InitChannelVarsApp, channel.id)
      )

      verify(channels).continue(channel.id, None, None, None, None, channel.id)
      testprobe.expectTerminated(fsmWithMock)
    }
  }
}

