package services.initChannelVars

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import ari.actions.Ari
import models.events.ws.{StasisEnd, StasisStart}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus.AriWsMessage
import services.rights.{RightsDbActions, RightsHelper}
import services.routing.{RoutingDbActions, RoutingHelper}
import services.{AriEventBus, DatabaseManagers}
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.ExecutionContext

class InitChannelVarsFsmManagerSpec extends AnyWordSpecLike with Matchers with MockitoSugar with Eventually {

  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = system.dispatcher

  class Helper() {
    val aribus = mock[AriEventBus]
    val ari = mock[Ari]
    val testprobe = TestProbe()

    val routeDbActions = mock[RoutingDbActions]
    val routingHelper = new RoutingHelper(aribus)

    val rightsDbActions = mock[RightsDbActions]
    val rightsHelper = new RightsHelper(rightsDbActions)

    val initChannelHelper = new InitChannelVarsHelper(aribus)

    val dbManagers = mock[DatabaseManagers]
    when(dbManagers.routeManager).thenReturn(testprobe.ref)
    when(dbManagers.rightsManager).thenReturn(testprobe.ref)
    when(dbManagers.initChannelVarsManager).thenReturn(testprobe.ref)

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val stasisStartEvent = StasisStart(List("1", "routePath"), channel, "08:00:27:80:c1:5f", "outcalls")
    val stasisEndEvent = StasisEnd(channel, "08:00:27:80:c1:5f", "outcalls")

    val managerActor = TestActorRef(new InitChannelFsmManager(aribus, rightsDbActions, rightsHelper, routeDbActions,
      routingHelper, initChannelHelper, dbManagers))

    val manager: InitChannelFsmManager = managerActor.underlyingActor
  }

  "InitChannelVarsFsmManager" should {
    "start and stop the fsm" in new Helper {
      managerActor ! AriWsMessage(stasisStartEvent)
      manager.initializedFsmIds.get("1549372028.17").isDefined shouldBe true

      managerActor ! AriWsMessage(stasisEndEvent)
      manager.initializedFsmIds.get("1549372028.17").isDefined shouldBe false
      manager.initializedFsmIds.get("1549372028.17").isEmpty shouldBe true

    }
  }
}
