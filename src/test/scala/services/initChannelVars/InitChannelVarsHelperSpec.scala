package services.initChannelVars

import models.ChannelVariables._
import models._
import models.events.bus.{ChannelVar, SetChannelVar}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus._
import services.initChannelVars.InitChannelVarsFsm.{SetRouteId, SetRouteSubroutine, SetUserRecordingFilename, SetXdsUserId}
import services.initChannelVars.InitChannelVarsHelper.ChannelVariable
import services.routing.RoutingDbActions.Route
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

class InitChannelVarsHelperSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  val TIMEOUT = 2000
  class Helper {
    val aribus = mock[AriEventBus]
    val helper = new InitChannelVarsHelper(aribus)

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    def getExpectedSetVars(actions: List[SetChannelVar]): List[AriEvent] = {
      actions.map(action =>
        AriEvent(
          AriTopic(TopicType.RESTREQUEST, DefaultApp),
          AriRequestMessage(action, InitChannelVarsApp, "1549372028.17")
        ))
    }
  }

  "InitChannelVarsHelper" should {
    "set channel var" in new Helper {
      val action = SetChannelVar("1549372028.17", ChannelVar("XIVO_DSTID", Some("12345")))
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, InitChannelVarsApp, "1549372028.17"))

      helper.setChannelVars(channel, "XIVO_DSTID", Some("12345"))
      verify(aribus, timeout(TIMEOUT)).publish(expected)
    }

    "set recording filename" in new Helper {
      import java.time.Instant
      val epoch = Instant.now.getEpochSecond
      val channelData = ChannelData(Map(SOURCE_NUMBER -> Some("1001"), DESTINATION_NUMBER -> Some("2001")))

      val filename = s"user-1001-2001-$epoch.wav"
      val action = SetChannelVar("1549372028.17", ChannelVar("XIVO_CALLRECORDFILE", Some(filename)))
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, InitChannelVarsApp, "1549372028.17"))

      helper.setUserRecordingFilename(channel, channelData, epoch) shouldBe List(SetUserRecordingFilename(CALL_RECORD_FILE_NAME))
      verify(aribus, timeout(TIMEOUT)).publish(expected)
    }

    "set route id with subroutine" in new Helper {
      val channelData = ChannelData(Map(SOURCE_NUMBER -> Some("1001"), DESTINATION_NUMBER -> Some("2001")))
      val route = Route(1, Some("subroutine"), None, None, None, false)

      helper.setRouteId(channel, route) shouldBe List(SetRouteId(XIVO_ROUTE_ID), SetRouteSubroutine(OUTCALL_PREPROCESS_SUBROUTINE))
    }

    "set anonymous caller id" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("CALLERID(name-pres)", Some("prohib"))),
        SetChannelVar("1549372028.17", ChannelVar("CALLERID(num-pres)", Some("prohib")))
      )

      val expected = getExpectedSetVars(actions)

      val varsToSet = helper.setCallerId(channel, Some("anonymous"), None, false)
      varsToSet.shouldBe(List(ChannelVariable("CALLERID(name-pres)", "prohib"), ChannelVariable("CALLERID(num-pres)", "prohib")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "set default caller id in full format" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("CALLERID(all)", Some("\"jirka <42100>\"")))
      )
      val expected = getExpectedSetVars(actions)

      val varsToSet = helper.setCallerId(channel, Some("default"), Some("\"jirka\" <42100>"), false)

      varsToSet.shouldBe(List(ChannelVariable("CALLERID(all)", "\"jirka <42100>\"")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "set custom caller id without name" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("CALLERID(all)", Some("\"42100 <42100>\"")))
      )
      val expected = getExpectedSetVars(actions)

      val varsToSet = helper.setCallerId(channel, Some("42100"), Some("\"forgot about\" <ddds00>"), false)

      varsToSet.shouldBe(List(ChannelVariable("CALLERID(all)", "\"42100 <42100>\"")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "set default caller id name only" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("CALLERID(name)", Some("\"name only\"")))
      )
      val expected = getExpectedSetVars(actions)

      val varsToSet = helper.setCallerId(channel, Some("default"), Some("\"name only\""), false)

      varsToSet.shouldBe(List(ChannelVariable("CALLERID(name)", "\"name only\"")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "set internal route caller id" in new Helper {
      val actions = List()

      val expected = getExpectedSetVars(actions)

      val varsToSet: List[ChannelVariable] = helper.setCallerId(channel, Some("default"), Some("\"name only\""), true)

      varsToSet.shouldBe(List())
      expected.foreach(result =>
        verifyNoInteractions(aribus, timeout(TIMEOUT)))
    }

    "set call option enable blind transfer" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("XIVO_CALLOPTIONS", Some("T")))
      )

      val expected: List[AriEvent] = getExpectedSetVars(actions)

      val internal = false
      val enableXfer = true
      val enableOnlineRec = false
      val varsToSet = helper.setCallOptionVars(channel, internal, enableXfer, enableOnlineRec)

      varsToSet.shouldBe(List(ChannelVariable("XIVO_CALLOPTIONS", "T")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "set call option enable online call recording" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("XIVO_CALLOPTIONS", Some("X")))
      )

      val expected: List[AriEvent] = getExpectedSetVars(actions)

      val internal = false
      val enableXfer = false
      val enableOnlineRec = true
      val varsToSet = helper.setCallOptionVars(channel, internal, enableXfer, enableOnlineRec)

      varsToSet.shouldBe(List(ChannelVariable("XIVO_CALLOPTIONS", "X")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "not set call option enable online call recording if route internal is set" in new Helper {
      val actions = List()

      val expected: List[AriEvent] = getExpectedSetVars(actions)

      val internal = true
      val enableXfer = false
      val enableOnlineRec = true
      val varsToSet = helper.setCallOptionVars(channel, internal, enableXfer, enableOnlineRec)

      varsToSet.shouldBe(List())
      expected.foreach(result =>
        verifyNoInteractions(aribus, timeout(TIMEOUT)))
    }

    "set call option enable online call recording and xfer" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("XIVO_CALLOPTIONS", Some("XT")))
      )

      val expected: List[AriEvent] = getExpectedSetVars(actions)

      val internal = false
      val enableXfer = true
      val enableOnlineRec = true
      val varsToSet = helper.setCallOptionVars(channel, internal, enableXfer, enableOnlineRec)

      varsToSet.shouldBe(List(ChannelVariable("XIVO_CALLOPTIONS", "XT")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }

    "set xds user id" in new Helper {
      val actions = List(
        SetChannelVar("1549372028.17", ChannelVar("XIVO_XDS_USERID", Some("1")))
      )

      val expected: List[AriEvent] = getExpectedSetVars(actions)

      val userId = "1"
      val varsToSet = helper.setXdsUserId(channel, userId)

      varsToSet.shouldBe(List(SetXdsUserId("XIVO_XDS_USERID")))
      expected.foreach(result =>
        verify(aribus, timeout(TIMEOUT)).publish(result))
    }
  }
}
