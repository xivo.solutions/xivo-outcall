package services

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{TestFSMRef, TestProbe}
import ari.actions.Ari
import models.events.bus.{ContinueInDialplan, SetChannelVar}
import models.events.ws.StasisStart
import models.{Channel, ChannelData, ChannelVar, ChannelVarResponse}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus._
import services.FsmFactory.{FsmContext, Initial}
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

class FsmFactorySpec extends AnyWordSpecLike with Matchers with MockitoSugar with Eventually {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    val testProbe = TestProbe()
    val aribus = mock[AriEventBus]
    val ari = new Ari(aribus, RoutingApp)
    val channelMock: Channel = mock[Channel]
    when(channelMock.id).thenReturn("123.45")

    class DummyFsm extends FsmFactory {
      override val channel: Channel = channelMock
      override val dbManagers = mock[DatabaseManagers]
      override val ari = new Ari(aribus, RoutingApp)

      startWith(Initial, FsmContext(List(), channel, ChannelData(Map())))

      when(Initial) {
        case Event(_: StasisStart, _: Context) =>
          stay()
      }
    }
  }

  "stop and continue in dialplan on unhandled event" in new Helper {
    val fsm = TestFSMRef(new DummyFsm)

    testProbe watch fsm

    val mustBeTypedProperly: TestFSMRef[FsmState, FsmData, DummyFsm] = fsm
    val response = ChannelVarResponse("DUMMY_VAR", ChannelVar(Some("1")))

    fsm ! response

    val continueAction = AriEvent(
      AriTopic(TopicType.RESTREQUEST, DefaultApp),
      AriRequestMessage(ContinueInDialplan("123.45", None, Some("failed"), Some(1), None), RoutingApp, "123.45")
    )
    val setAction = continueAction.copy(message = AriRequestMessage(
      SetChannelVar("123.45", models.events.bus.ChannelVar("XIVO_OUTCALL_ERROR", Some(s"state Initial failed with error: received unhandled request $response"))), RoutingApp, "123.45"
    ))

    verify(aribus).publish(continueAction)
    verify(aribus).publish(setAction)

    testProbe.expectTerminated(fsm)
  }
}
