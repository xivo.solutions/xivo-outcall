package services.rights

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{TestFSMRef, TestProbe}
import ari.actions.Ari
import models.ChannelVariables.*
import models.*
import models.events.bus.ContinueInDialplan
import models.events.ws.{ChannelVarset, StasisStart}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus.*
import services.FsmFactory.{Initial, Uninitialized}
import services.rights.RightsDbActions.{RightCall, RightCallExten, User}
import services.rights.RightsDbManager.*
import services.rights.RightsFsm.*
import services.{AriEventBus, DatabaseManagers, FsmData, FsmState}
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any
import scala.concurrent.Future

class RightsFsmSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  implicit val system: ActorSystem = ActorSystem()
  val TIMEOUT = 2000

  val testProbe = TestProbe()

  val aribus = mock[AriEventBus]
  val ari = new Ari(aribus, RightsApp)
  val rightsHelper = mock[RightsHelper]
  val repository = mock[RightsFsmRepository]

  val dbManagers = mock[DatabaseManagers]
  when(dbManagers.rightsManager).thenReturn(testProbe.ref)

  val caller = Caller(Some("User One"), Some("1001"))
  val connected = Connected(Some(""), Some(""))
  val dialplan = Dialplan("default", "9999", 3)
  val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

  val stasisStart = StasisStart(List(), channel, "08:00:27:80:c1:5f", "schedules")

  val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
  val mustBeTypedProperly: TestFSMRef[FsmState, FsmData, RightsFsm] = fsm

  "RightsFsm" should {
    "get required channel variables" in {
      val repository = new RightsFsmRepository
      repository.getRequiredChannelVars shouldBe List("XIVO_ROUTE_ID", "XIVO_DSTNUM", "XIVO_CALLFORWARDED", "FORWARDERNAME",
        "XIVO_FWD_REFERER", "XIVO_FWD_ACTION", "XIVO_FWD_ACTIONARG1")
    }

    "start in Initial state" in {
      assert(fsm.stateName == Initial)
      assert(fsm.stateData == Uninitialized)
    }

    "transition to AwaitUserId state" in {
      fsm ! stasisStart

      val waitingVars = RightsFsmContext(ChannelData(Map()), List(), None, channel)

      assert(fsm.stateName == AwaitUserId)
      assert(fsm.stateData == waitingVars)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitChannelVars state" in {
      val response = ChannelVarResponse("XIVO_USERID", new ChannelVar(Some("1")))
      when(repository.getRequiredChannelVars).thenReturn(List("XIVO_DSTNUM"))

      fsm ! response

      val waitingVars = RightsFsmContext(ChannelData(Map("XIVO_USERID" -> Some("1"))), repository.getRequiredChannelVars, None, channel)

      assert(fsm.stateName == AwaitChannelVars)
      assert(fsm.stateData == waitingVars)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitRightsIds state" in {
      val response = ChannelVarResponse("XIVO_DSTNUM", new ChannelVar(Some("00123")))
      val channelVars = Map("XIVO_USERID" -> Some("1"), "XIVO_DSTNUM" -> Some("00123"))

      when(rightsHelper.isCallForwarded(channelVars)).thenReturn(None)
      when(rightsHelper.getDstNumForRoute(channelVars, None)).thenReturn(Future.successful(Some("00123")))

      fsm ! response

      assert(fsm.stateName == AwaitRightsIds)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to UserHasRight state" in {
      val exten = RightCallExten(1, "1000")
      val channelVars = Map("XIVO_USERID" -> Some("1"), "XIVO_DSTNUM" -> Some("00123"))
      val ctxData = ChannelData(channelVars)

      val richRightCall = RichRightsFsmContext(List(exten), Some("00123"), List(exten), Some(User(1, None, false, "test", false, false)), List(), ctxData, channel)

      fsm ! richRightCall

      testProbe.expectMsg(GetRightsIds(RightsFsmContextWitUserId(ctxData, None, None, channel)))
      testProbe.expectMsg(GetUser(1))

      fsm ! ResponseUser(Some(User(1, None, false, "test", false, false)))

      assert(fsm.stateName == UserHasRight)
      assert(fsm.stateData == richRightCall)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitSetRights" in {
      val exten = RightCallExten(1, "1000")
      val channelVars = Map("XIVO_USERID" -> Some("1"), "XIVO_DSTNUM" -> Some("00123"))
      val ctxData = ChannelData(channelVars)

      val richRightCall = RichRightsFsmContext(List(exten), Some("00123"), List(), Some(User(1, None, false, "test", false, false)), List(), ctxData, channel)
      val waitingVars = RightsFsmContext(ctxData, List("XIVO_AUTHORIZATION"), None, channel)

      val deny = RightCall(0, "")

      val response = ResponseRightCalls(RightsFound(List(deny)))

      testProbe.expectMsg(GetRightCallUser("1", richRightCall.rightCallsIds))

      fsm ! response

      assert(fsm.stateName == AwaitSetRights)
      assert(fsm.stateData == waitingVars)
      assert(fsm.isStateTimerActive == true)
    }

    "continue in dialplan and stop" in {
      reset(aribus)
      testProbe watch fsm

      val response = ChannelVarset("XIVO_AUTHORIZATION", "DENY", channel, "asterisk-123", "DefaultApp")

      fsm ! response

      testProbe.expectTerminated(fsm)

      val action = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, None, None, None), RightsApp, channel.id)
      )

      verify(aribus).publish(action)
    }

    "process ForwardXivo and transition to AwaitRightsIds state" in {
      val channelData = Map("XIVO_FWD_REFERER" -> Some("user:1"), "XIVO_USERID" -> Some("1"))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ChannelVarResponse("XIVO_FWD_REFERER", new ChannelVar(Some("user:1")))

      val waitingVars = RightsFsmContext(ChannelData(channelData), List(), None, channel)

      when(rightsHelper.isCallForwarded(channelData)).thenReturn(Some(ForwardXivo))

      fsm.setState(AwaitChannelVars, waitingVars)

      fsm ! response

      testProbe.expectMsg(GetRightsIds(RightsFsmContextWitUserId(waitingVars.channelData, Some("1"), Some(ForwardXivo), channel)))
      assert(fsm.stateName == AwaitRightsIds)

      fsm.stop()
    }

    "process ForwardXivo with unexpected referer and transition to AwaitRightsIds state" in {
      val channelData = Map("XIVO_FWD_REFERER" -> Some("unexpected:1"), "XIVO_USERID" -> Some("1"))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ChannelVarResponse("XIVO_FWD_REFERER", new ChannelVar(Some("unexpected:1")))

      val waitingVars = RightsFsmContext(ChannelData(channelData), List(), None, channel)

      when(rightsHelper.isCallForwarded(channelData)).thenReturn(Some(ForwardXivo))

      fsm.setState(AwaitChannelVars, waitingVars)

      fsm ! response

      testProbe.expectMsg(GetRightsIds(RightsFsmContextWitUserId(waitingVars.channelData, None, None, channel)))
      assert(fsm.stateName == AwaitRightsIds)

      fsm.stop()
    }

    "process ForwardSip and transition to AwaitRightsIds state" in {
      val channelData = Map(FORWARDERNAME -> Some("someforwarder"), "XIVO_USERID" -> Some("1"))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ChannelVarResponse("FORWARDERNAME", new ChannelVar(Some("someforwarder")))

      val waitingVars = RightsFsmContext(ChannelData(channelData), List(), None, channel)

      when(rightsHelper.isCallForwarded(channelData)).thenReturn(Some(ForwardSip))
      when(rightsHelper.getIdFromChannel("someforwarder")).thenReturn(Future.successful(ForwardedUser("19")))

      fsm.setState(AwaitChannelVars, waitingVars)

      fsm ! response

      testProbe.expectMsg(GetRightsIds(RightsFsmContextWitUserId(waitingVars.channelData, Some("19"), Some(ForwardSip), channel)))
      assert(fsm.stateName == AwaitRightsIds)

      fsm.stop()
    }

    "process forwarding without XIVO_FWD_REFERER and transition to AwaitRightsIds state" in {
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ChannelVarResponse("XIVO_USERID", new ChannelVar(Some("1")))

      val waitingVars = RightsFsmContext(ChannelData(Map("XIVO_USERID" -> Some("1"))), List(), None, channel)

      when(rightsHelper.isCallForwarded(Map("XIVO_USERID" -> Some("1")))).thenReturn(Some(ForwardXivo))

      fsm.setState(AwaitChannelVars, waitingVars)

      fsm ! response

      testProbe.expectMsg(GetRightsIds(RightsFsmContextWitUserId(waitingVars.channelData, None, None, channel)))
      assert(fsm.stateName == AwaitRightsIds)

      fsm.stop()
    }

    "process forwarding without FORWARDERNAME and transition to AwaitRightsIds state" in {
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ChannelVarResponse("XIVO_USERID", new ChannelVar(Some("1")))

      val waitingVars = RightsFsmContext(ChannelData(Map("XIVO_USERID" -> Some("1"))), List(), None, channel)

      when(rightsHelper.isCallForwarded(Map("XIVO_USERID" -> Some("1")))).thenReturn(Some(ForwardSip))

      fsm.setState(AwaitChannelVars, waitingVars)

      fsm ! response

      testProbe.expectMsg(GetRightsIds(RightsFsmContextWitUserId(waitingVars.channelData, None, None, channel)))
      assert(fsm.stateName == AwaitRightsIds)

      fsm.stop()
    }

    "allow if no call rights are set" in {
      reset(aribus)
      val channelData = Map("XIVO_USERID" -> Some("1"))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"), List(), None,
        List(), ChannelData(Map("XIVO_USERID" -> Some("1"))), channel)

      val waitingVars = RightsFsmContext(ChannelData(channelData), List("XIVO_AUTHORIZATION"), None, channel)

      fsm.setState(AwaitRightsIds, waitingVars)

      fsm ! response
      fsm ! ChannelVarset("XIVO_AUTHORIZATION", "ALLOW", channel, "asterisk-123", "DefaultApp")

      val action = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, None, None, None), RightsApp, channel.id)
      )

      verify(aribus).publish(action)
    }

    "allow if no user and no routePath are set" in {
      reset(aribus)
      val channelData = Map("XIVO_USERID" -> Some("1"))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), ChannelData(Map("XIVO_USERID" -> Some("1"))), channel)

      val waitingVars = RightsFsmContext(ChannelData(channelData), List("XIVO_AUTHORIZATION"), None, channel)

      fsm.setState(AwaitRightsIds, waitingVars)

      fsm ! response
      fsm ! ChannelVarset("XIVO_AUTHORIZATION", "ALLOW", channel, "asterisk-123", "DefaultApp")

      val action = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, None, None, None), RightsApp, channel.id)
      )

      verify(aribus).publish(action)

      fsm.stop()
    }

    "find rights for group" in {
      reset(aribus)
      val channelData = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ResponseRightCalls(RightsFound(List(RightCall(0, ""))))

      val ctx = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), channelData, channel)

      fsm.setState(GroupOrRouteHasRight, ctx)

      fsm ! response

      testProbe.expectNoMessage()

      assert(fsm.stateName == AwaitSetRights)
      assert(fsm.stateData == RightsFsmContext(channelData, List(XIVO_AUTHORIZATION), None, channel))
      fsm.stop()

      verify(aribus, timeout(TIMEOUT).times(1)).publish(any[AriEvent])
    }

    "not find rights for group" in {
      reset(aribus)
      val channelData = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ResponseRightCalls(RightsNotFound)

      val ctx = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), channelData, channel)

      fsm.setState(GroupOrRouteHasRight, ctx)

      fsm ! response

      testProbe.expectMsg(GetRightCallRoute(ctx.channelData.getChannelVar(XIVO_ROUTE_ID).get, ctx.validatedRightCallsIds))
      assert(fsm.stateName == AwaitRightsForRoute)
      fsm.stop()

    }

    "fail to find rights for group" in {
      reset(aribus)
      val channelData = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ResponseRightCalls(RightsFailed(new Exception("query failed")))

      val ctx = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), channelData, channel)

      fsm.setState(GroupOrRouteHasRight, ctx)

      fsm ! response

      testProbe.expectMsg(GetRightCallRoute(ctx.channelData.getChannelVar(XIVO_ROUTE_ID).get, ctx.validatedRightCallsIds))
      assert(fsm.stateName == AwaitRightsForRoute)
      fsm.stop()

      verifyNoInteractions(aribus)
    }

    "find rights for route" in {
      reset(aribus)
      val channelData = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ResponseRightCalls(RightsFound(List(RightCall(0, ""))))

      val ctx = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), channelData, channel)

      fsm.setState(AwaitRightsForRoute, ctx)

      fsm ! response

      testProbe.expectNoMessage()

      assert(fsm.stateName == AwaitSetRights)
      assert(fsm.stateData == RightsFsmContext(channelData, List(XIVO_AUTHORIZATION), None, channel))
      fsm.stop()

      verify(aribus, timeout(TIMEOUT).times(1)).publish(any[AriEvent])
    }

    "not find rights for route" in {
      reset(aribus)
      val channelData = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ResponseRightCalls(RightsNotFound)

      val ctx = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), channelData, channel)

      fsm.setState(AwaitRightsForRoute, ctx)

      fsm ! response

      assert(fsm.stateName == AwaitSetRights)
      assert(fsm.stateData == RightsFsmContext(channelData, List(XIVO_AUTHORIZATION), None, channel))
      fsm.stop()

      verify(aribus, timeout(TIMEOUT).times(1)).publish(any[AriEvent])
    }

    "fail to find rights for route" in {
      reset(aribus)
      val watcherProbe = TestProbe()
      val channelData = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))
      val fsm = TestFSMRef(new RightsFsm(aribus, ari, dbManagers, rightsHelper, repository, channel))
      val response = ResponseRightCalls(RightsFailed(new Exception("query failed")))

      watcherProbe watch fsm

      val ctx = RichRightsFsmContext(List(RightCallExten(1, "0123X.")), Some("012300424242"),
        List(RightCallExten(1, "0123X.")), None,
        List(), channelData, channel)

      fsm.setState(AwaitRightsForRoute, ctx)

      fsm ! response

      verify(aribus, timeout(TIMEOUT).times(2)).publish(any[AriEvent])
      watcherProbe.expectTerminated(fsm)
    }
  }
}
