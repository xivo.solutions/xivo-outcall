package services.rights

import models.ChannelVariables._
import models._
import models.events.bus.{ChannelVar, SetChannelVar}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus._
import services.rights.RightsDbActions.RightCall
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

class RightsActionSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  class Helper {
    val ariBus = mock[AriEventBus]
    val actions = new RightsAction(ariBus, "123")

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("123.45", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

  }

  "RightsAction" should {
    "disallow with password if call right has autorization" in new Helper {
      val action = SetChannelVar("123.45", ChannelVar("XIVO_AUTHORIZATION", Some("DENY")))
      val event = AriRequestMessage(action, RightsApp, "123")
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), event)

      val right = RightCall(0, "password")
      actions.applyRules(channel, List(right)) shouldBe List(
        CallPassword(XIVO_PASSWORD, "password"),
        CallAuthorization(XIVO_AUTHORIZATION, DENY)
      )
      verify(ariBus, timeout(2000)).publish(expected)
    }

    "disallow without password" in new Helper {
      val action = SetChannelVar("123.45", ChannelVar("XIVO_AUTHORIZATION", Some("DENY")))
      val event = AriRequestMessage(action, RightsApp, "123")
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), event)

      val right = RightCall(0, "")
      actions.applyRules(channel, List(right)) shouldBe List(CallAuthorization(XIVO_AUTHORIZATION, DENY))
      verify(ariBus, timeout(2000)).publish(expected)
    }

    "allow without call right" in new Helper {
      val action = SetChannelVar("123.45", ChannelVar("XIVO_AUTHORIZATION", Some("ALLOW")))
      val event = AriRequestMessage(action, RightsApp, "123")
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), event)

      actions.applyRules(channel, List()) shouldBe List(CallAuthorization(XIVO_AUTHORIZATION, ALLOW))
      verify(ariBus, timeout(2000)).publish(expected)
    }

    "allow with call right" in new Helper {
      val action = SetChannelVar("123.45", ChannelVar("XIVO_AUTHORIZATION", Some("ALLOW")))
      val event = AriRequestMessage(action, RightsApp, "123")
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), event)

      val right = RightCall(1, "")
      actions.applyRules(channel, List(right)) shouldBe List(CallAuthorization(XIVO_AUTHORIZATION, ALLOW))
      verify(ariBus, timeout(2000)).publish(expected)
    }
  }

}
