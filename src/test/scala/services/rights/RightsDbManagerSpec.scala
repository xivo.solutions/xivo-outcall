package services.rights

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{ImplicitSender, TestKit}
import models._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.rights.RightsDbActions.{GroupFeatures, RightCall, RightCallExten, User}
import services.rights.RightsDbManager._
import services.rights.RightsFsm.{RichRightsFsmContext, RightsFsmContextWitUserId}
import org.mockito.Mockito.when

import scala.concurrent.Future

class RightsDbManagerSpec extends TestKit(ActorSystem("MySpec")) with ImplicitSender with AnyWordSpecLike with Matchers
    with MockitoSugar with ScalaFutures with BeforeAndAfterAll {

  class Helper() {
    val dbActions = mock[RightsDbActions]
    val rightsHelper = mock[RightsHelper]

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = models.Channel("1549618160.0", "SIP/7c7d0cda-00000000", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val rightCallExten = RightCallExten(1, "012300")
    val rightCall = RightCall(0, "123")

    val dbManagerActor = system.actorOf(RightsDbManager.props(dbActions, rightsHelper))
  }

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "RightsDbManager" should {
    "get rights ids" in new Helper {
      val dst = Some("012300")

      when(dbActions.getRightCallExten).thenReturn(Future.successful(List(rightCallExten)))
      when(dbActions.getUser(1)).thenReturn(Future.successful(Some(User(1, None, false, "outcallerId", false, false))))
      when(rightsHelper.getDstNumForRoute(Map(), None)).thenReturn(Future.successful(dst))
      when(rightsHelper.getIdsForDestination(List(rightCallExten), dst)).thenReturn(List(rightCallExten))

      dbManagerActor ! GetRightsIds(RightsFsmContextWitUserId(ChannelData(Map()), Some("1"), None, channel))

      val expected = RichRightsFsmContext(List(rightCallExten), dst, List(RightCallExten(1, "012300")), Some(User(1, None, false, "outcallerId", false, false)), List(), ChannelData(Map()), channel)
      expectMsg(expected)
    }

    "get user" in new Helper {
      when(dbActions.getUser(1)).thenReturn(Future.successful(Some(User(1, None, false, "test", false, false))))

      dbManagerActor ! GetUser(1)

      val expected = ResponseUser(Some(User(1, None, false, "test", false, false)))
      expectMsg(expected)
    }

    "get right call for user" in new Helper {
      when(dbActions.getRightCallUser("1", List(rightCallExten))).thenReturn(Future.successful(List(rightCall)))

      dbManagerActor ! GetRightCallUser("1", List(rightCallExten))

      val expected = ResponseRightCalls(RightsFound(List(rightCall)))
      expectMsg(expected)

    }

    "get right call for groups" in new Helper {
      when(dbActions.getGroupFeatures(1)).thenReturn(Future.successful(List(GroupFeatures(1))))
      when(dbActions.getGroupRights(List(1), List(rightCallExten))).thenReturn(Future.successful(List(rightCall)))

      dbManagerActor ! GetGroupRights(1, List(rightCallExten))

      val expected = ResponseRightCalls(RightsFound(List(rightCall)))
      expectMsg(expected)
    }

    "get empty right call for groups" in new Helper {
      when(dbActions.getGroupFeatures(1)).thenReturn(Future.successful(List()))

      dbManagerActor ! GetGroupRights(1, List(rightCallExten))

      val expected = ResponseRightCalls(RightsNotFound)
      expectMsg(expected)
    }

    "get empty right call for groups if query failed" in new Helper {
      val toThrow = new Exception("query failed")

      when(dbActions.getGroupFeatures(1)).thenReturn(Future.failed(toThrow))

      dbManagerActor ! GetGroupRights(1, List(rightCallExten))

      val expected = ResponseRightCalls(RightsFailed(toThrow))
      expectMsg(expected)
    }

    "get right call for routes" in new Helper {
      when(dbActions.getRightCallRoute("1", List(rightCallExten))).thenReturn(Future.successful(List(rightCall)))

      dbManagerActor ! GetRightCallRoute("1", List(rightCallExten))

      val expected = ResponseRightCalls(RightsFound(List(rightCall)))
      expectMsg(expected)
    }
  }
}
