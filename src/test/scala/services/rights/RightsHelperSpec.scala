package services.rights

import org.apache.pekko.actor.ActorSystem
import models.ChannelVariables.*
import models.*
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.rights.RightsDbActions.{Extension, LineFeatures, RightCallExten, UserLine}
import services.rights.RightsFsm.*
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.{ExecutionContext, Future}

class RightsHelperSpec extends AnyWordSpecLike with Matchers with MockitoSugar with ScalaFutures {

  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = system.dispatcher

  class Helper {
    val db: RightsDbActions = mock[RightsDbActions]
    val helper = new RightsHelper(db)

  }

  "RightsHelper" should {
    "check call is not forwarded" in new Helper {
      val channelData: Map[String, Option[String]] = Map()
      helper.isCallForwarded(channelData) shouldBe None
    }

    "check call is xivo type forwarded" in new Helper {
      val channelData: Map[String, Option[String]] = Map(CALLFORWARDED -> Some("1"))
      helper.isCallForwarded(channelData) shouldBe Some(ForwardXivo)
    }

    "check call is sip type forwarded" in new Helper {
      val channelData: Map[String, Option[String]] = Map(FORWARDERNAME -> Some("sip/abcfd"))
      helper.isCallForwarded(channelData) shouldBe Some(ForwardSip)
    }

    "get destination name for routePath" in new Helper {
      val channelVars: Map[String, Option[String]] = Map(
        OUTCALL_ID -> Some("1"),
        DESTINATION_NUMBER -> Some("01230042101")
      )

      whenReady(helper.getDstNumForRoute(channelVars, None)) { result =>
        result shouldBe Some("01230042101")
      }
    }

    "get destination name for others without forward" in new Helper {
      val channelVars: Map[String, Option[String]] = Map(
        DESTINATION_NUMBER -> Some("01230042101")
      )

      whenReady(helper.getDstNumForRoute(channelVars, None)) { result =>
        result shouldBe Some("01230042101")
      }
    }

    "get destination name for others (extension) with xivo forward" in new Helper {
      val channelVars: Map[String, Option[String]] = Map(
        DESTINATION_NUMBER -> Some("01230042101"),
        XIVO_FWD_ACTION -> Some("extension"),
        XIVO_FWD_ACTIONARG1 -> Some("123456")
      )

      whenReady(helper.getDstNumForRoute(channelVars, Some(ForwardXivo))) { result =>
        result shouldBe Some("123456")
      }
    }

    "get destination name for others (user) with xivo forward" in new Helper {
      val channelVars: Map[String, Option[String]] = Map(
        DESTINATION_NUMBER -> Some("01230042101"),
        XIVO_FWD_ACTION -> Some("user"),
        XIVO_FWD_ACTIONARG1 -> Some("1")
      )

      when(db.getMainExtensionByUserId("1")).thenReturn(Future.successful(Some(Extension(1, 123456, "default"))))

      whenReady(helper.getDstNumForRoute(channelVars, Some(ForwardXivo))) { result =>
        result shouldBe Some("123456")
      }
    }

    "get destination name for others with sip forward" in new Helper {
      val channelVars: Map[String, Option[String]] = Map(
        DESTINATION_NUMBER -> Some("01230042101")
      )

      whenReady(helper.getDstNumForRoute(channelVars, Some(ForwardSip))) { result =>
        result shouldBe Some("01230042101")
      }
    }

    "get id from the channel" in new Helper {
      val forwarder = "sip/mgpweoie1"
      when(db.getLine("mgpweoie1")).thenReturn(Future.successful(Some(LineFeatures(1, "mgpweoie1", "1001", "default"))))
      when(db.getUserLine(1)).thenReturn(Future.successful(Some(UserLine(1, 1, 1, 1, true, true))))

      whenReady(helper.getIdFromChannel(forwarder)) { result =>
        result shouldBe ForwardedUser("1")
      }
    }

    "get id from the wrong channel" in new Helper {
      val forwarder = "wrongchannelname"
      when(db.getLine("mgpweoie1")).thenReturn(Future.successful(Some(LineFeatures(1, "mgpweoie1", "1001", "default"))))
      when(db.getUserLine(1)).thenReturn(Future.successful(Some(UserLine(1, 1, 1, 1, true, true))))

      whenReady(helper.getIdFromChannel(forwarder)) { result =>
        result shouldBe ForwardedElseWhere
      }
    }

    "get channel var in context data" in new Helper {
      val channelVars: Map[String, Option[String]] = Map()
      val ctx = ChannelData(channelVars).addChannelVar("XIVO_USERID", Some("1"))
      ctx.getChannelVar("XIVO_USERID") shouldBe Some("1")
    }

    "remove channel var from waiting vars" in new Helper {
      val channelVars: Map[String, Option[String]] = Map("XIVO_USERID" -> Some("1"))
      val waitingChannelVars = List("VAR1", "VAR2")
      val waitingContext = RightsFsmContext(ChannelData(channelVars), waitingChannelVars, None, mock[Channel])

      val newWaitingChannelVars = ChannelData(channelVars).removeOneAwaited("VAR1", waitingContext.awaiting)
      newWaitingChannelVars shouldBe List("VAR2")
    }

    "get rights ids" in new Helper {
      val dst = Some("012300")
      val rightCallExten = RightCallExten(1, "012300")
      helper.getIdsForDestination(List(rightCallExten), dst) shouldBe List(rightCallExten)
    }

    "get rights ids for different destination" in new Helper {
      val dst = Some("07891")
      val rightCallExten = RightCallExten(1, "012300")
      helper.getIdsForDestination(List(rightCallExten), dst) shouldBe List()
    }
  }
}
