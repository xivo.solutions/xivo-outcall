package services

import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class ExtPatternMatchingSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  "ExtPatternMatching" should {
    "match" in {
      ExtPatternMatching.isMatch("1XXX", "1004") shouldBe true
      ExtPatternMatching.isMatch("10X", "100") shouldBe true
      ExtPatternMatching.isMatch("01230042101", "01230042101") shouldBe true
      ExtPatternMatching.isMatch("0123X.", "01230042101") shouldBe true
      ExtPatternMatching.isMatch("+33[1-5]XXXXXXXX", "+33112345678") shouldBe true
      ExtPatternMatching.isMatch("+ZX", "+12") shouldBe true
      ExtPatternMatching.isMatch("00ZX.", "00123456") shouldBe true
      ExtPatternMatching.isMatch("+336XXXXXXXX", "+33612345678") shouldBe true
      ExtPatternMatching.isMatch("+337[3-9]XXXXXXX", "+33732345678") shouldBe true
      ExtPatternMatching.isMatch("07[3-9]XXXXXXX", "0731234567") shouldBe true
      ExtPatternMatching.isMatch("2Z!", "210123**#123") shouldBe true
    }

    "not match" in {
      ExtPatternMatching.isMatch("1XXX", "123") shouldBe false
      ExtPatternMatching.isMatch("1XXX", "2001") shouldBe false
      ExtPatternMatching.isMatch("10X", "1000") shouldBe false
      ExtPatternMatching.isMatch("0123X.", "0123") shouldBe false
      ExtPatternMatching.isMatch("0123X.", "01230") shouldBe false
      ExtPatternMatching.isMatch("+33[1-5]XXXXXXXX", "33112345678") shouldBe false
      ExtPatternMatching.isMatch("+ZX", "+02") shouldBe false
      ExtPatternMatching.isMatch("00ZX.", "00023456") shouldBe false
      ExtPatternMatching.isMatch("+336XXXXXXXX", "+33512345678") shouldBe false
      ExtPatternMatching.isMatch("+337[3-9]XXXXXXX", "+33712345678") shouldBe false
      ExtPatternMatching.isMatch("07[3-9]XXXXXXX", "0721234567") shouldBe false
      ExtPatternMatching.isMatch("2Z!", "200123**#123") shouldBe false
    }
  }
}
