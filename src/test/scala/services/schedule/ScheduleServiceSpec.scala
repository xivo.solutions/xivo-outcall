package services.schedule

import org.apache.pekko.actor.{ActorSystem, Props, Status}
import org.apache.pekko.testkit.{TestKit, TestProbe}
import app.Configuration
import ari.actions.{Ari, Channels}
import models.ChannelVariables.XIVO_OUTCALL_ERROR
import models.events.ws.StasisStart
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus.DefaultApp
import services.schedule.ScheduleCalendar._
import services.schedule.ScheduleService.RoutePath
import services.schedule.ScheduleServiceManager.ContinueInDialplan
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.{ExecutionContextExecutor, Future}

class ScheduleServiceSpec extends AnyWordSpecLike with Matchers with MockitoSugar
  with BeforeAndAfterAll {

  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  class Helper() {
    val calendar = mock[ScheduleCalendar]
    val config: Configuration = mock[Configuration]
    val ariBus: AriEventBus = mock[AriEventBus]

    val ariChannels = mock[Channels]
    val scheduleExecutor = mock[ScheduleActionExecutor]
    val ari = new Ari(ariBus, DefaultApp) {
      override val channels: Channels = ariChannels
    }

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val stasisStartEvent = StasisStart(List("1", "routePath"), channel, "08:00:27:80:c1:5f", "outcalls")
    val stasisStartEventNoArgs = StasisStart(List(), channel, "08:00:27:80:c1:5f", "outcalls")

    val defaultScheduleAction = EndcallHangup("endcall:hangup", None, None)

    val parent: TestProbe = TestProbe()
    val scheduleFsmActor = parent.childActorOf(Props(classOf[ScheduleService], ariBus, ari, calendar, channel, scheduleExecutor))
  }

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "ScheduleService" should {
    "process schedule action if no arguments in stasis event" in new Helper {
      val openedWithoutAction = Schedule(ScheduleStateOpened, ScheduleActionNone)

      when(calendar.processSchedule(None, channel)).thenReturn(Future.successful(openedWithoutAction))

      scheduleFsmActor ! stasisStartEventNoArgs

      verify(scheduleExecutor, timeout(200)).processSchedulePeriod(openedWithoutAction, channel)
    }

    "execute the default action" in new Helper {
      val closedWithoutAction = Schedule(ScheduleStateClosed, defaultScheduleAction)
      val path = RoutePath("routePath", 1)

      when(calendar.defaultAction).thenReturn(EndcallHangup("endcall:hangup", None, None))
      when(calendar.processSchedule(Some(path), channel)).thenReturn(Future.successful(closedWithoutAction))

      scheduleFsmActor ! stasisStartEvent

      verify(scheduleExecutor, timeout(200)).processSchedulePeriod(closedWithoutAction, channel)
    }

    "handle failure when processing the schedule" in new Helper {
      val e = new Exception("error")
      when(calendar.processSchedule(None, channel)).thenReturn(Future.failed(e))

      scheduleFsmActor ! stasisStartEventNoArgs

      parent.expectMsg(Status.Failure(e))
    }

    "continue in dialplan if requested" in new Helper {
      scheduleFsmActor ! ContinueInDialplan("error")

      verify(ari.channels).setChannelVar(channel.id, XIVO_OUTCALL_ERROR, Some("error"), channel.id)
      verify(ari.channels).continue(channel.id, None, Some("failed"), Some(1), None, channel.id)
    }
  }
}
