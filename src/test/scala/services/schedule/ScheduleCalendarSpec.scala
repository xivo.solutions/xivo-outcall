package services.schedule

import org.apache.pekko.actor.ActorSystem

import java.time.{Clock, LocalDateTime, OffsetDateTime}
import models.Channel
import org.joda.time.{DateTime, LocalTime}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.schedule.ScheduleCalendar.*
import services.schedule.ScheduleDbManager.{ModeClosed, ModeOpened, SchedulePath, UncheckedSchedulePeriods}
import services.schedule.ScheduleService.RoutePath
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.{ExecutionContext, Future}

class ScheduleCalendarSpec extends AnyWordSpecLike with Matchers with MockitoSugar with ScalaFutures {

  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = system.dispatcher

  class Helper() {
    private val year = 2019
    private val month = 2
    private val day = 27
    private val hour = 11
    private val minute = 30

    val mockClock: Clock = Clock.fixed(
      LocalDateTime.of(year, month, day, hour, minute)
        .toInstant(OffsetDateTime.now().getOffset),
      Clock.systemDefaultZone().getZone
    )

    val db = mock[ScheduleDbManager]
    val calendar = new ScheduleCalendar(db, mockClock)
    val calendarParser = calendar.parser

    val scheduleHours = ScheduleHours("08:30", "16:30")
    val schedulePath = SchedulePath(1, "Europe/Paris", ScheduleActionNone, "a schedule")
    val scheduleWeekdays = List(1, 2, 3, 5)
    val scheduleMonthDays = List(1, 2, 3, 4, 5, 6, 7, 8, 19)
    val scheduleMonths = List(1, 2, 3, 12)

    val scheduleHoursNow = LocalTime.now
    val scheduleHourStart = scheduleHoursNow.minusHours(4).toString
    val scheduleHourStop = scheduleHoursNow.plusHours(4).toString

    val defaultAction = ScheduleActionNone

    val openedSchedulePeriods = UncheckedSchedulePeriods(ModeOpened, s"$scheduleHourStart-$scheduleHourStop", None, None, None, ScheduleActionNone)
    val closedSchedulePeriods = UncheckedSchedulePeriods(ModeClosed, s"$scheduleHourStart-$scheduleHourStop", None, None, None, ScheduleActionNone)

    val closedSchedule = Schedule(ScheduleStateClosed, defaultAction, Some("a schedule"))
    val openedSchedule = Schedule(ScheduleStateOpened, defaultAction, Some("a schedule"))

  }

  "ScheduleCalendar" should {
    "split hours" in new Helper {
      val hours = "08:30-16:30"
      calendarParser.splitToHour(hours) shouldBe Some(scheduleHours)
    }

    "not split hours" in new Helper {
      val hours = "08:3016:30"
      calendarParser.splitToHour(hours) shouldBe None
    }

    "split weekdays" in new Helper {
      val weekdays = Some("1-3,5")
      calendarParser.splitWeekDays(weekdays) shouldBe scheduleWeekdays
    }

    "not split weekdays" in new Helper {
      val weekdays = None
      calendarParser.splitWeekDays(weekdays) shouldBe List()
    }

    "split days of months" in new Helper {
      val days = Some("1-8,19")
      calendarParser.splitMonthDays(days) shouldBe scheduleMonthDays
    }

    "not split days of months" in new Helper {
      val days = None
      calendarParser.splitMonthDays(days) shouldBe List()
    }

    "split months" in new Helper {
      val months = Some("1-3,12")
      calendarParser.splitMonths(months) shouldBe scheduleMonths
    }

    "not split months" in new Helper {
      val months = None
      calendarParser.splitMonths(months) shouldBe List()
    }

    "check if current time is within the period" in new Helper {
      val start = "09:30"
      val stop = "12:30"
      val hours = ScheduleHours(start, stop)

      calendarParser.isHoursWithinInterval(Some(hours)) shouldBe true
    }

    "check if current time is outside the period" in new Helper {
      val start = "04:30"
      val stop = "07:30"
      val hours = ScheduleHours(start, stop)

      calendarParser.isHoursWithinInterval(Some(hours)) shouldBe false
    }

    "be closed if the current schedule does not have any month, day and weekday specified" in new Helper {
      val routePath = RoutePath("routePath", 1)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(openedSchedulePeriods)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe closedSchedule
    }

    "be closed if call is within closed period" in new Helper {
      val routePath = RoutePath("routePath", 1)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(closedSchedulePeriods)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe closedSchedule
    }

    "be opened if there is no schedule path" in new Helper {
      val routePath = RoutePath("routePath", 1)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(None))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe Schedule(ScheduleStateOpened, defaultAction, None)
    }

    "be closed if call is within closed period during opened period" in new Helper {
      val routePath = RoutePath("routePath", 1)
      val startOpen: String = scheduleHoursNow.minusHours(6).toString
      val stopOpen: String = scheduleHoursNow.plusHours(6).toString

      val startClose: String = scheduleHoursNow.minusHours(1).toString
      val stopClose: String = scheduleHoursNow.plusHours(2).toString

      val startWeekday = new DateTime().minusDays(1).getDayOfWeek
      val stopWeekday = new DateTime().plusDays(1).getDayOfWeek

      val startDate = new DateTime().minusDays(1).getDayOfMonth
      val (stopDate, stopDateNext) = (new DateTime().plusDays(1).getDayOfMonth, new DateTime().plusDays(2).getDayOfMonth)

      val startMonth = new DateTime().minusMonths(1).getMonthOfYear
      val (endMonth, endMonthNext) = (new DateTime().plusMonths(1).getMonthOfYear, new DateTime().plusMonths(2).getMonthOfYear)

      val openPeriod = UncheckedSchedulePeriods(ModeOpened, s"$startOpen-$stopOpen",
        weekdays = Some(s"$startWeekday-$stopWeekday"),
        monthdays = Some(s"$startDate-$stopDate"),
        months = Some(s"$startMonth-$endMonth"), ScheduleActionNone)
      val closedPeriod = UncheckedSchedulePeriods(ModeClosed, s"$startClose-$stopClose",
        weekdays = Some(s"$startWeekday-$stopWeekday"),
        monthdays = Some(s"$startDate-$stopDate"),
        months = Some(s"$startMonth-$endMonth"), ScheduleActionNone)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(openPeriod, closedPeriod)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe Schedule(ScheduleStateClosed, ScheduleActionNone, Some("a schedule"))
    }

    "be closed if call is outside open hours" in new Helper {
      val routePath = RoutePath("routePath", 1)
      val start = scheduleHoursNow.minusHours(6).toString
      val stop = scheduleHoursNow.minusHours(4).toString
      val openPeriod = UncheckedSchedulePeriods(ModeOpened, s"$start-$stop", None, None, None, ScheduleActionNone)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(openPeriod)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe closedSchedule
    }

    "be opened if call is inside complex open hours" in new Helper {
      val routePath = RoutePath("routePath", 1)

      val openPeriod = UncheckedSchedulePeriods(mode = ModeOpened, hours = s"9:35-21:25",
        weekdays = Some(s"1-5"),
        monthdays = Some(s"24-27,28"),
        months = Some(s"1-2,3"), ScheduleActionNone)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(openPeriod)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe Schedule(ScheduleStateOpened, ScheduleActionNone, Some("a schedule"))
    }

    "be closed if call is outside complex open hours" in new Helper {
      val routePath = RoutePath("routePath", 1)

      val openPeriod = UncheckedSchedulePeriods(mode = ModeOpened, hours = s"1:35-07:25",
        weekdays = Some(s"1-5"),
        monthdays = Some(s"24-27,28"),
        months = Some(s"1-2,3"), ScheduleActionNone)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(openPeriod)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe closedSchedule
    }

    "handle overlapping February" in new Helper {
      val routePath = RoutePath("routePath", 1)
      val start = scheduleHoursNow.minusHours(6).toString
      val stop = scheduleHoursNow.plusHours(4).toString

      val startWeekday: Int = new DateTime().minusDays(1).getDayOfWeek
      val stopWeekday: Int = new DateTime().plusDays(1).getDayOfWeek

      val date: Int = 31
      val month: Int = 2

      val openPeriod = UncheckedSchedulePeriods(mode = ModeOpened, hours = s"$start-$stop",
        weekdays = Some(s"$startWeekday-$stopWeekday"),
        monthdays = Some(s"$date"),
        months = Some(s"$month"), ScheduleActionNone)

      when(db.getSchedulePath(routePath)).thenReturn(Future.successful(Some(schedulePath)))
      when(db.getSchedulePeriods(schedulePath)).thenReturn(Future.successful(List(openPeriod)))

      calendar.processSchedule(Some(routePath), mock[Channel]).futureValue shouldBe closedSchedule
    }
  }
}
