package services.schedule

import org.apache.pekko.actor.ActorSystem
import app.Configuration
import ari.actions.Channels.HangupReason
import ari.actions.{Ari, Channels}
import models.events.bus.{ChannelVar, ContinueInDialplan, HangupChannel, SetChannelVar}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus._
import services.schedule.ScheduleCalendar._
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

class ScheduleActionExecutorSpec extends AnyWordSpecLike with Matchers with MockitoSugar with Eventually {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()

    val config: Configuration = mock[Configuration]
    val ariBus: AriEventBus = mock[AriEventBus]

    val ariChannels = mock[Channels]
    val ari = new Ari(ariBus, DefaultApp)

    val channelId = "1549372028.17"

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val scheduleAction = EndcallHangup("endcall:hangup", None, None)
    val defaultScheduleAction = EndcallHangup("endcall:hangup", None, None)
    val scheduleClosed = Schedule(ScheduleStateClosed, scheduleAction)

    val actionExec = new ScheduleActionExecutor(ari)
  }

  "ScheduleActionExecutor" should {
    "continue in dialplan if no arguments in stasis event" in new Helper {
      val openedWithoutAction = Schedule(ScheduleStateOpened, ScheduleActionNone)
      val action = ContinueInDialplan("1549372028.17", None, None, None, None)
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, DefaultApp, channelId))

      actionExec.processSchedulePeriod(openedWithoutAction, channel)

      verify(ariBus).publish(expected)
    }

    "erase the channel variable XIVO_SCHEDULE_STATUS" in new Helper {
      val openedWithoutAction = Schedule(ScheduleStateOpened, ScheduleActionNone)
      val action = SetChannelVar("1549372028.17", ChannelVar("XIVO_SCHEDULE_STATUS", None))
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, DefaultApp, channelId))

      actionExec.processSchedulePeriod(openedWithoutAction, channel)

      verify(ariBus).publish(expected)
    }

    "execute the default action" in new Helper {
      val closedWithoutAction = Schedule(ScheduleStateClosed, defaultScheduleAction)
      val action = HangupChannel("1549372028.17", Some(HangupReason.busy))
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, DefaultApp, channelId))

      actionExec.processSchedulePeriod(closedWithoutAction, channel)

      verify(ariBus).publish(expected)
    }

    "set channel variables based on the action" in new Helper {
      val userId = "1"
      val closedWithUser = Schedule(ScheduleStateClosed, ProcessInDialplan("user", Some(userId), Some("25")))

      val channelVar1 = SetChannelVar("1549372028.17", ChannelVar("XIVO_SCHEDULE_STATUS", Some("closed")))
      val channelVar2 = SetChannelVar("1549372028.17", ChannelVar("XIVO_PATH", Some("")))
      val channelVar3 = SetChannelVar("1549372028.17", ChannelVar("XIVO_FWD_SCHEDULE_OUT_ACTION", Some("user")))
      val channelVar4 = SetChannelVar("1549372028.17", ChannelVar("XIVO_FWD_SCHEDULE_OUT_ACTIONARG1", Some(userId)))
      val channelVar5 = SetChannelVar("1549372028.17", ChannelVar("XIVO_FWD_SCHEDULE_OUT_ACTIONARG2", Some("25")))
      val channelContinue = ContinueInDialplan("1549372028.17", None, None, None, None)

      val expected1 = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(channelVar1, DefaultApp, channelId))
      val expected2 = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(channelVar1, DefaultApp, channelId))
      val expected3 = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(channelVar1, DefaultApp, channelId))
      val expected4 = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(channelVar1, DefaultApp, channelId))
      val expected5 = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(channelVar1, DefaultApp, channelId))
      val expected6 = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(channelContinue, DefaultApp, channelId))

      actionExec.processSchedulePeriod(closedWithUser, channel)

      verify(ariBus).publish(expected1)
      verify(ariBus).publish(expected2)
      verify(ariBus).publish(expected3)
      verify(ariBus).publish(expected4)
      verify(ariBus).publish(expected5)
      verify(ariBus).publish(expected5)
      verify(ariBus).publish(expected6)
    }

    "hangup the call on schedule closed" in new Helper {
      val action = HangupChannel("1549372028.17", Some(HangupReason.busy))
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, DefaultApp, channelId))

      actionExec.processSchedulePeriod(scheduleClosed, channel)

      verify(ariBus).publish(expected)
    }
  }
}
