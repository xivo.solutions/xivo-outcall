package services.schedule

import org.apache.pekko.actor.{ActorSystem, Status}
import org.apache.pekko.testkit.{DefaultTimeout, TestActorRef, TestKit, TestProbe}
import app.Configuration
import models.events.ws.{StasisEnd, StasisStart}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus.{AriResponseMessage, AriWsMessage}
import services.schedule.ScheduleServiceManager.ContinueInDialplan

import scala.concurrent.ExecutionContextExecutor

class ScheduleServiceManagerSpec extends TestKit(_system = ActorSystem()) with AnyWordSpecLike with Matchers
  with MockitoSugar with DefaultTimeout with BeforeAndAfterAll {

  class Helper() {
    implicit val ec: ExecutionContextExecutor = system.dispatcher

    val db = mock[ScheduleDbManager]
    val config: Configuration = mock[Configuration]
    val ariBus: AriEventBus = mock[AriEventBus]
    val scheduleCalendar = mock[ScheduleCalendar]

    val probe: TestProbe = TestProbe()

    val scheduleFsmActor: TestActorRef[ScheduleServiceManager] = TestActorRef(new ScheduleServiceManager(ariBus, scheduleCalendar) {
      override val scheduleActionExecutor = mock[ScheduleActionExecutor]
    })
    val scheduleManager: ScheduleServiceManager = scheduleFsmActor.underlyingActor

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val stasisStartEvent = StasisStart(List("1", "routePath"), channel, "08:00:27:80:c1:5f", "outcalls")
    val stasisEndEvent = StasisEnd(channel, "08:00:27:80:c1:5f", "outcalls")
  }

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "ScheduleFsmManager" should {
    "create new service on StasisStart" in new Helper {
      scheduleManager.receive(AriWsMessage(stasisStartEvent))

      scheduleManager.initializedServiceIds.size shouldBe 1
    }

    "forward message to correct service" in new Helper {
      val message = AriResponseMessage(channel, "123.45")
      scheduleManager.initializedServiceIds += "123.45" -> probe.ref
      scheduleManager.receive(message)

      probe.expectMsg(message.answer)
    }

    "handle response message if fsm id not in list" in new Helper {
      val message = AriResponseMessage(channel, "123.45")
      scheduleManager.receive(message)

      scheduleManager.initializedServiceIds.size shouldBe 0
    }

    "terminate channel service on StasisEnd" in new Helper {
      scheduleManager.receive(AriWsMessage(stasisStartEvent))

      scheduleManager.initializedServiceIds.size shouldBe 1

      val watched = scheduleManager.initializedServiceIds.head._2
      probe watch watched

      scheduleManager.receive(AriWsMessage(stasisEndEvent))

      scheduleManager.initializedServiceIds.size shouldBe 0
      probe.expectTerminated(watched)
    }

    "send back to dialplan if schedule service status is failed" in new Helper {
      probe.send(scheduleFsmActor, Status.Failure(new Exception("error")))

      probe.expectMsg(ContinueInDialplan("error"))
    }
  }
}
