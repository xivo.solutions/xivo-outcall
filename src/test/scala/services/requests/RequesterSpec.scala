package services.requests

import org.apache.pekko.actor.{ActorSystem, PoisonPill}
import org.apache.pekko.http.scaladsl.model.headers.Authorization
import org.apache.pekko.http.scaladsl.model.{StatusCodes, *}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import app.Configuration
import ari.actions.Channels.HangupReason
import models.events.bus.*
import models.{Caller, Connected, Dialplan}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus.*
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.*

class RequesterSpec extends AnyWordSpecLike with Matchers with MockitoSugar with ScalaFutures with Eventually with BeforeAndAfterAll {
  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = system.dispatcher

  class Helper() {
    val config: Configuration = mock[Configuration]
    val ariRest: AriRest = mock[AriRest]
    val ariBus: AriEventBus = mock[AriEventBus]

    val subscription: AriEventBus.ScheduleApp.type = ScheduleApp

    val requesterActor = TestActorRef(new Requester(config, ariRest, ariBus))
    val requester: Requester = requesterActor.underlyingActor

    val authorization: Authorization = requester.authorization

    val channelJson = """[{"id":"1549618160.0","name":"SIP/7c7d0cda-00000000","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-05T14:07:08.895+0100","language":"en_US"}]"""
    val errorJson = """{"some": "error"}"""

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = models.Channel("1549618160.0", "SIP/7c7d0cda-00000000", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    when(config.ariHost).thenReturn("example")
    when(config.ariPort).thenReturn("8088")
    when(config.ariUsername).thenReturn("user")
    when(config.ariPassword).thenReturn("pass")

    case object UnkownRequest
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.terminate()
  }

  "Requester" should {
    "create uri to connect to" in new Helper {
      val expectedUrl = "http://example:8088/ari/channels"
      requester.createUri("channels") shouldEqual expectedUrl
    }

    "process response entity Channel" in new Helper {
      val entity = HttpEntity(channelJson)
      val resp = HttpResponse(status = StatusCodes.OK, entity = entity)
      whenReady(requester.processResponse[List[models.Channel]](resp)) { result =>
        result shouldBe List(channel)
      }
    }

    "process request and publish response on GetChanels" in new Helper {
      val action = GetChannels
      val msg = AriRequestMessage(action, subscription, "123.45")
      val expectedUrl = "http://example:8088/ari/channels"

      val entity = HttpEntity(channelJson)
      val resp = HttpResponse(status = StatusCodes.OK, entity = entity)

      when(ariRest.singleRequest(expectedUrl, HttpMethods.GET, authorization))
        .thenReturn(Future.successful(resp))

      requester.receive(msg)

      verify(ariRest).singleRequest(expectedUrl, HttpMethods.GET, authorization)
      eventually(timeout(scaled(500.milliseconds))) {
        verify(ariBus).publish(AriEvent(AriTopic(TopicType.RESTRESPONSE, subscription), AriResponseMessage(channel, "123.45")))
      }
    }

    "process request and not publish on HangupChannel" in new Helper {
      reset(ariBus)
      val action = HangupChannel("123.45", Some(HangupReason.busy))
      val msg = AriRequestMessage(action, subscription, "123.45")
      val expectedUrl = "http://example:8088/ari/channels/123.45?reason=busy"

      val entity = HttpEntity(channelJson)
      val resp = HttpResponse(status = StatusCodes.OK, entity = entity)

      when(ariRest.singleRequest(expectedUrl, HttpMethods.DELETE, authorization))
        .thenReturn(Future.successful(resp))

      requester.receive(msg)

      verify(ariRest).singleRequest(expectedUrl, HttpMethods.DELETE, authorization)
      eventually(timeout(scaled(500.milliseconds))) {
        verifyNoInteractions(ariBus)
      }
    }

    "process request and not publish on ContinueInDialplan" in new Helper {
      reset(ariBus)
      val action = ContinueInDialplan("123.45", None, None, None, None)
      val msg = AriRequestMessage(action, subscription, "123.45")
      val expectedUrl = "http://example:8088/ari/channels/123.45/continue?context=&extension=&priority=&label="

      val resp = HttpResponse(status = StatusCodes.OK)

      when(ariRest.singleRequest(expectedUrl, HttpMethods.POST, authorization))
        .thenReturn(Future.successful(resp))

      requester.receive(msg)

      verify(ariRest).singleRequest(expectedUrl, HttpMethods.POST, authorization)
      eventually(timeout(scaled(500.milliseconds))) {
        verifyNoInteractions(ariBus)
      }
    }

    "process request and not publish response on SetChannelVar" in new Helper {
      reset(ariBus)
      val action = SetChannelVar("123.45", ChannelVar("TEST", Some("TEST")))
      val msg = AriRequestMessage(action, subscription, "123.45")
      val expectedUrl = "http://example:8088/ari/channels/123.45/variable?variable=TEST&value=TEST"

      val resp = HttpResponse(status = StatusCodes.OK)

      when(ariRest.singleRequest(expectedUrl, HttpMethods.POST, authorization))
        .thenReturn(Future.successful(resp))

      requester.receive(msg)

      verify(ariRest).singleRequest(expectedUrl, HttpMethods.POST, authorization)
      eventually(timeout(scaled(500.milliseconds))) {
        verifyNoInteractions(ariBus)
      }
    }

    "process request and publish response on SetChannelVar without value" in new Helper {
      val action = SetChannelVar("123.45", ChannelVar("TEST", None))
      val msg = AriRequestMessage(action, subscription, "123.45")
      val expectedUrl = "http://example:8088/ari/channels/123.45/variable?variable=TEST&value="

      val resp = HttpResponse(status = StatusCodes.OK)

      when(ariRest.singleRequest(expectedUrl, HttpMethods.POST, authorization))
        .thenReturn(Future.successful(resp))

      requester.receive(msg)

      verify(ariRest).singleRequest(expectedUrl, HttpMethods.POST, authorization)
    }

    "should handle an exception when request with response fails" in new Helper {
      val entity = HttpEntity(errorJson)
      val resp = HttpResponse(status = StatusCodes.OK, entity = entity)

      requester.processResponse[models.Channel](resp).failed.futureValue shouldBe an[Exception]
    }

    "handle malformed response entity" in new Helper {
      val entity = HttpEntity(errorJson)
      val resp = HttpResponse(status = StatusCodes.OK, entity = entity)

      requester.processResponse[models.Channel](resp).failed.futureValue shouldBe an[Exception]
    }

    "handle unknown bus event" in new Helper {
      reset(ariRest)
      reset(ariBus)

      requester.receive(UnkownRequest)

      verifyNoInteractions(ariRest)
      verifyNoInteractions(ariBus)
    }

    "terminate the requester" in new Helper {
      val probe = TestProbe()

      probe watch requesterActor

      eventually(timeout(scaled(1000.milliseconds))) {
        requesterActor ! PoisonPill
        probe.expectTerminated(requesterActor)
      }
    }
  }
}
