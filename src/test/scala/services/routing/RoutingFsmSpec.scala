package services.routing

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{TestFSMRef, TestProbe}
import ari.actions.Ari
import models.ChannelVariables._
import models._
import models.events.bus.{ContinueInDialplan, SetChannelVar}
import models.events.ws.{ChannelVarset, StasisStart}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus._
import services.FsmFactory.{FsmContext, Initial, Uninitialized}
import services.routing.RoutingDbActions._
import services.routing.RoutingDbManager._
import services.routing.RoutingFsm._
import services.{AriEventBus, DatabaseManagers}
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.duration._

class RoutingFsmSpec extends AnyWordSpecLike with Matchers with MockitoSugar with Eventually {

  implicit val system: ActorSystem = ActorSystem()

  val TIMEOUT = 3150.milliseconds

  val testProbe = TestProbe()

  val aribus = mock[AriEventBus]
  val helper = new RoutingHelper(aribus)
  val ari = new Ari(aribus, RoutingApp)

  val dbManagers = mock[DatabaseManagers]
  when(dbManagers.routeManager).thenReturn(testProbe.ref)

  val caller = Caller(Some("User One"), Some("1001"))
  val connected = Connected(Some(""), Some(""))
  val dialplan = Dialplan("default", "9999", 3)
  val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

  val stasisStart = StasisStart(List(), channel, "08:00:27:80:c1:5f", "schedules")
  val route = Route(1, None, None, None, None, false)

  "RoutingFsm" should {
    val fsm = TestFSMRef(new RoutingFsm(ari, dbManagers, helper, channel))

    "start in Initial state" in {
      assert(fsm.stateName == Initial)
      assert(fsm.stateData == Uninitialized)
    }

    "transition to AwaitChannelVars state" in {
      fsm ! stasisStart

      val channelData = ChannelData(Map("MDS_NAME" -> Some("default")))
      val fsmCtx = FsmContext(List(DESTINATION_NUMBER, XIVO_BASE_CONTEXT), channel, channelData)

      assert(fsm.stateName == AwaitChannelVars)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitRoute state" in {
      val response1 = ChannelVarResponse("XIVO_DSTNUM", new ChannelVar(Some("0123456")))
      val response2 = ChannelVarResponse("XIVO_BASE_CONTEXT", new ChannelVar(Some("default")))

      fsm ! response1
      fsm ! response2

      val channelData = ChannelData(Map(
        "XIVO_DSTNUM" -> Some("0123456"),
        "XIVO_BASE_CONTEXT" -> Some("default"),
        "MDS_NAME" -> Some("default")
      ))

      val fsmCtx = FsmContext(List(), channel, channelData)

      testProbe.expectMsg(GetRoute(fsmCtx.channelData))

      assert(fsm.stateName == AwaitRoute)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitRouteTrunk state" in {
      val response = ResponseRoute(List(Route(1, None, None, None, None, false)))

      fsm ! response

      val channelData = ChannelData(Map(
        "XIVO_DSTNUM" -> Some("0123456"),
        "XIVO_BASE_CONTEXT" -> Some("default"),
        "MDS_NAME" -> Some("default")
      ))

      val fsmCtx = FsmContext(List(), channel, channelData)

      testProbe.expectMsg(GetRouteTrunk(response.routes))

      assert(fsm.stateName == AwaitRouteTrunk)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitInterface state" in {
      val response = ResponseRouteTrunk(Map(Route(1, None, None, None, None, false) -> List(
        RouteTrunk(1, 1, Sip, 1, 1, "default", TrunkOnCurrentMds),
        RouteTrunk(1, 2, Sip, 1, 2, "mds1", TrunkOnCurrentMds),
        RouteTrunk(1, 3, Sip, 1, 1, "default", TrunkOnCurrentMds)
      )))

      fsm ! response

      val channelData = ChannelData(Map(
        "XIVO_DSTNUM" -> Some("0123456"),
        "XIVO_BASE_CONTEXT" -> Some("default"),
        "MDS_NAME" -> Some("default")
      ))

      val fsmCtx = FsmContext(List(), channel, channelData)

      testProbe.expectMsg(GetTrunkInterface(List(
        RouteTrunk(1, 1, Sip, 1, 1, "default", TrunkOnCurrentMds),
        RouteTrunk(1, 2, Sip, 1, 2, "mds1", TrunkOnAnotherMds),
        RouteTrunk(1, 3, Sip, 1, 1, "default", TrunkOnCurrentMds)
      ), route))

      assert(fsm.stateName == AwaitInterface)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "transition to AwaitSetTrunkInDialplan state" in {
      val response = ResponseTrunkInterface(List(TrunkInterface(TrunkSip(1, "abcdefg"), "SIP/abcdefg", None)), route)

      fsm ! response

      val channelData = ChannelData(Map(
        "XIVO_DSTNUM" -> Some("0123456"),
        "XIVO_BASE_CONTEXT" -> Some("default"),
        "MDS_NAME" -> Some("default")
      ))

      val fsmCtx = FsmContext(List(s"${INTERFACE}0", s"${TRUNK_EXTEN}0", s"${TRUNK_GOTOMDS}0"), channel, channelData)

      assert(fsm.stateName == AwaitSetTrunkInDialplan)
      assert(fsm.stateData == fsmCtx)
      assert(fsm.isStateTimerActive == true)
    }

    "continue in dialplan and stop" in {
      reset(aribus)
      testProbe watch fsm

      val response1 = ChannelVarset(s"${INTERFACE}0", "SIP/abcdefg", channel, "asterisk-123", "routing")
      val response2 = ChannelVarset(s"${TRUNK_EXTEN}0", "0123456", channel, "asterisk-123", "routing")
      val response3 = ChannelVarset(s"${TRUNK_GOTOMDS}0", "true", channel, "asterisk-123", "routing")

      fsm ! response1
      fsm ! response2
      fsm ! response3

      val action = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, None, None, None), RoutingApp, channel.id)
      )

      verify(aribus).publish(action)
      testProbe.expectTerminated(fsm)
    }

    "continue in failed dialplan after state timeout" in {
      reset(aribus)
      val fsm = TestFSMRef(new RoutingFsm(ari, dbManagers, helper, channel))
      val fsmCtx = FsmContext(List(s"${INTERFACE}0", s"${TRUNK_EXTEN}0"), channel, ChannelData(Map()))

      testProbe watch fsm

      fsm.setState(AwaitSetTrunkInDialplan, fsmCtx)

      val response1 = ChannelVarset(s"${INTERFACE}0", "SIP/abcdefg", channel, "asterisk-123", "routing")

      fsm ! response1

      val continueAction = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, Some("failed"), Some(1), None), RoutingApp, channel.id)
      )

      val setAction = continueAction.copy(message = AriRequestMessage(
        SetChannelVar(channel.id, models.events.bus.ChannelVar("XIVO_OUTCALL_ERROR", Some("state AwaitSetTrunkInDialplan failed with error: state timeout"))), RoutingApp, channel.id
      ))

      eventually(timeout(scaled(TIMEOUT))) {
        verify(aribus).publish(continueAction)
        verify(aribus).publish(setAction)
      }

      testProbe.expectTerminated(fsm)
    }

    "continue in failed dialplan if no trunk is enabled" in {
      reset(aribus)
      val fsm = TestFSMRef(new RoutingFsm(ari, dbManagers, helper, channel))
      val fsmCtx = FsmContext(List(), channel, ChannelData(Map()))

      testProbe watch fsm

      fsm.setState(AwaitInterface, fsmCtx)

      val response = ResponseTrunkInterface(List(), route)

      fsm ! response

      val continueAction = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, Some("failed"), Some(1), None), RoutingApp, channel.id)
      )

      val setAction = continueAction.copy(message = AriRequestMessage(
        SetChannelVar(channel.id, models.events.bus.ChannelVar("XIVO_OUTCALL_ERROR", Some(s"state AwaitInterface failed with error: no enabled trunks found in route $route"))), RoutingApp, channel.id
      ))

      eventually(timeout(scaled(TIMEOUT))) {
        verify(aribus).publish(continueAction)
        verify(aribus).publish(setAction)
      }

      testProbe.expectTerminated(fsm)
    }

    "continue in failed dialplan if no trunk is available" in {
      reset(aribus)
      val fsm = TestFSMRef(new RoutingFsm(ari, dbManagers, helper, channel))
      val fsmCtx = FsmContext(List(), channel, ChannelData(Map()))

      testProbe watch fsm

      fsm.setState(AwaitRouteTrunk, fsmCtx)

      val routeTrunkMap = Map(Route(1, None, None, None, None, false) -> List())
      val response = ResponseRouteTrunk(routeTrunkMap)

      fsm ! response

      val continueAction = AriEvent(
        AriTopic(TopicType.RESTREQUEST, DefaultApp),
        AriRequestMessage(ContinueInDialplan(channel.id, None, Some("failed"), Some(1), None), RoutingApp, channel.id)
      )

      val setAction: AriEvent = continueAction.copy(message = AriRequestMessage(
        SetChannelVar(channel.id, models.events.bus.ChannelVar("XIVO_OUTCALL_ERROR", Some(s"state AwaitRouteTrunk failed with error: no trunks found in route ${routeTrunkMap.head}"))), RoutingApp, channel.id
      ))

      eventually(timeout(scaled(TIMEOUT))) {
        verify(aribus).publish(continueAction)
        verify(aribus).publish(setAction)
      }

      testProbe.expectTerminated(fsm)
    }
  }
}
