package services.routing

import models.ChannelVariables._
import models._
import models.events.bus.{ChannelVar, SetChannelVar}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus._
import services.routing.RoutingDbActions._
import services.routing.RoutingFsm.{SetTrunkExten, SetTrunkGotoMds, SetTrunkInterface, SetTrunkSuffix}
import services.routing.RoutingHelper.RouteInputData
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

class RoutingHelperSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  class Helper() {
    val ariEventBus: AriEventBus = mock[AriEventBus]

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val helper = new RoutingHelper(ariEventBus)
    val channelData = ChannelData(Map())
    val route = Route(1, None, None, None, None, false)
  }

  "RoutingHelper" should {
    "add channel var" in new Helper {
      val newChannelData = channelData.addChannelVar("var1", Some("value1"))
      newChannelData.vars shouldBe Map("var1" -> Some("value1"))
    }

    "get channel var" in new Helper {
      val newChannelData = channelData
        .addChannelVar("var1", Some("value1"))
        .addChannelVar("var2", Some("value2"))

      newChannelData.getChannelVar("var2") shouldBe Some("value2")
      newChannelData.getChannelVar("var3") shouldBe None
    }

    "remove one waited var" in new Helper {
      val waiting = List("waiting1", "waiting2")
      channelData.removeOneAwaited("waiting1", waiting) shouldBe List("waiting2")
    }

    "get input data for route" in new Helper {
      val newChannelData = channelData
        .addChannelVar(DESTINATION_NUMBER, Some("1001"))
        .addChannelVar(MDS_NAME, Some("mds0"))
        .addChannelVar(XIVO_BASE_CONTEXT, Some("default"))

      helper.getDataForRoute(newChannelData) shouldBe Some(RouteInputData("mds0", "1001", "default"))
    }

    "set channel var" in new Helper {
      val action = SetChannelVar("1549372028.17", ChannelVar("XIVO_DSTID", Some("12345")))
      val expected = AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, RoutingApp, channel.id))

      helper.setChannelVars(channel, "XIVO_DSTID", Some("12345"))
      verify(ariEventBus, timeout(2000)).publish(expected)
    }

    "process sip trunk interface" in new Helper {
      val dstNum = "1001"

      val interfaces = List(TrunkInterface(TrunkSip(1, "abcdefg"), "SIP/abcdefg", None))

      helper.processTrunkInterfaces(channel, dstNum, interfaces, route) shouldBe List(
        SetTrunkInterface(s"${INTERFACE}0"), SetTrunkExten(s"${TRUNK_EXTEN}0", "1001"), SetTrunkGotoMds(s"${TRUNK_GOTOMDS}0")
      )
    }

    "process iax trunk interface" in new Helper {
      val dstNum = "1001"

      val interfaces = List(TrunkInterface(TrunkIax(1, "abcdefg"), "IAX/abcdefg", None))
      helper.processTrunkInterfaces(channel, dstNum, interfaces, route) shouldBe List(
        SetTrunkInterface(s"${INTERFACE}0"), SetTrunkExten(s"${TRUNK_EXTEN}0", "1001"), SetTrunkGotoMds(s"${TRUNK_GOTOMDS}0")
      )
    }

    "process custom trunk interface" in new Helper {
      val dstNum = "1001"

      val interfaces = List(TrunkInterface(TrunkCustom(1, "abcdefg", "suffix"), "custom/abcdefg", None))
      helper.processTrunkInterfaces(channel, dstNum, interfaces, route) shouldBe List(
        SetTrunkInterface(s"${INTERFACE}0"), SetTrunkExten(s"${TRUNK_EXTEN}0", "1001"), SetTrunkSuffix(s"${TRUNK_SUFFIX}0")
      )
    }

    "transform destination" in new Helper {
      val routeAllFeatures = Route(1, None, Some(""".{6}(.*)"""), Some("""123\1"""), None, false)
      helper.regexTransformation(routeAllFeatures, "01230042101") shouldBe "12342101"

      val routeOnlyPrefix = Route(1, None, None, Some("""123\1"""), None, false)
      helper.regexTransformation(routeOnlyPrefix, "01230042101") shouldBe "12301230042101"

      val routeOnlyStripnum = Route(1, None, Some(""".{6}(.*)"""), None, None, false)
      helper.regexTransformation(routeOnlyStripnum, "01230042101") shouldBe "42101"
    }

    "not transform destination if trunk on another mds" in new Helper {
      val routeWithTransformation = Route(1, None, Some(""".{6}(.*)"""), None, None, false)
      val dstNum = "01230042101"

      val interfaces = List(
        TrunkInterface(TrunkSip(1, "abcdefg"), "SIP/abcdefg", None),
        TrunkInterface(TrunkSip(1, "abcdefg"), "SIP/abcdefg", None, TrunkOnAnotherMds)
      )

      helper.processTrunkInterfaces(channel, dstNum, interfaces, routeWithTransformation) shouldBe List(
        SetTrunkInterface(s"${INTERFACE}0"), SetTrunkExten(s"${TRUNK_EXTEN}0", "42101"), SetTrunkGotoMds(s"${TRUNK_GOTOMDS}0"),
        SetTrunkInterface(s"${INTERFACE}1"), SetTrunkExten(s"${TRUNK_EXTEN}1", "01230042101"), SetTrunkGotoMds(s"${TRUNK_GOTOMDS}1")
      )
    }
  }
}
