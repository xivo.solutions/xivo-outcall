package services.routing

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.testkit.{ImplicitSender, TestKit}
import models.ChannelVariables.{DESTINATION_NUMBER, MDS_NAME, XIVO_BASE_CONTEXT}
import models._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.routing.RoutingDbActions._
import services.routing.RoutingDbManager._
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.Future

class RoutingDbManagerSpec extends TestKit(ActorSystem("MySpec")) with ImplicitSender with AnyWordSpecLike with Matchers
    with MockitoSugar with ScalaFutures with BeforeAndAfterAll {

  class Helper() {
    val aribus = mock[AriEventBus]
    val dbActions = mock[RoutingDbActions]
    val helper = new RoutingHelper(aribus)

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = models.Channel("1549618160.0", "SIP/7c7d0cda-00000000", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val sip = TrunkSip(1, "abcd")
    val iax = TrunkIax(1, "abcd")
    val custom = TrunkCustom(1, "SIP/abcd", "suffix")

    val route = Route(1, None, None, None, None, false)
    def routeTrunk(protocol: Protocol, location: TrunkLocation) = RouteTrunk(1, 1, protocol, 1, 1, "mds0", location)

    val dbManagerActor: ActorRef = system.actorOf(RoutingDbManager.props(dbActions, helper))

  }

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "RightsDbManager" should {
    "get sip trunk interface" in new Helper {
      val trunk = TrunkSip(1, "abcdefgh")

      when(dbActions.getSipTrunk(1)).thenReturn(Future.successful(Some(trunk)))

      dbManagerActor ! GetTrunkInterface(List(routeTrunk(Sip, TrunkOnCurrentMds)), route)

      val expected = ResponseTrunkInterface(List(TrunkInterface(trunk, "SIP/abcdefgh", None)), route)
      expectMsg(expected)
    }

    "get iax trunk interface" in new Helper {
      val trunk = TrunkIax(1, "abcdefgh")

      when(dbActions.getIaxTrunk(1)).thenReturn(Future.successful(Some(trunk)))

      dbManagerActor ! GetTrunkInterface(List(routeTrunk(Iax, TrunkOnCurrentMds)), route)

      val expected = ResponseTrunkInterface(List(TrunkInterface(trunk, "IAX/abcdefgh", None)), route)
      expectMsg(expected)
    }

    "get custom trunk interface" in new Helper {
      val trunk = TrunkCustom(1, "SIP/custom", "suffix")

      when(dbActions.getCustomTrunk(1)).thenReturn(Future.successful(Some(trunk)))

      dbManagerActor ! GetTrunkInterface(List(routeTrunk(Custom, TrunkOnCurrentMds)), route)

      val expected = ResponseTrunkInterface(List(TrunkInterface(trunk, "SIP/custom", Some("suffix"))), route)
      expectMsg(expected)
    }

    "get route" in new Helper {
      val ctx = ChannelData(Map(
        DESTINATION_NUMBER -> Some("1001"),
        MDS_NAME -> Some("mds0"),
        XIVO_BASE_CONTEXT -> Some("default")
      ))
      val mds = MediaServer(1, "mds0", "10.0.0.1")

      when(dbActions.getMediaserver("mds0")).thenReturn(Future.successful(Some(mds)))
      when(dbActions.getRouteByPattern("1001", "default", mds)).thenReturn(Future.successful(List(route)))

      dbManagerActor ! GetRoute(ctx)

      val expected = ResponseRoute(List(Route(1, None, None, None, None, internalCallerId = false)))
      expectMsg(expected)
    }

    "get route trunk" in new Helper {
      val routes = List(Route(1, None, None, None, None, internalCallerId = false), Route(2, None, None, None, None, internalCallerId = false))
      val rt1 = RouteTrunk(1, 1, Sip, 1, 1, "mds0", TrunkOnCurrentMds)
      val rt2 = RouteTrunk(1, 2, Iax, 1, 2, "mds0", TrunkOnCurrentMds)
      val rt3 = RouteTrunk(2, 3, Custom, 1, 2, "mds0", TrunkOnCurrentMds)
      val rt4 = RouteTrunk(2, 4, Sip, 1, 2, "mds0", TrunkOnCurrentMds)

      when(dbActions.getRouteTrunk(1)).thenReturn(Future.successful(List(rt1, rt2)))
      when(dbActions.getRouteTrunk(2)).thenReturn(Future.successful(List(rt3, rt4)))

      dbManagerActor ! GetRouteTrunk(routes)

      val res = Map(routes(0) -> List(rt1, rt2), routes(1) -> List(rt3, rt4))

      val expected = ResponseRouteTrunk(res)
      expectMsg(expected)
    }

    "get trunk interfaces" in new Helper {
      val rt1 = RouteTrunk(1, 1, Sip, 1, 1, "mds0", TrunkOnCurrentMds)
      val rt2 = RouteTrunk(1, 2, Iax, 1, 2, "mds0", TrunkOnCurrentMds)
      val rt3 = RouteTrunk(2, 3, Custom, 1, 2, "mds0", TrunkOnCurrentMds)
      val rt4 = RouteTrunk(2, 4, Sip, 1, 2, "mds0", TrunkOnAnotherMds)
      val routeTrunks = List(rt1, rt2, rt3, rt4)

      when(dbActions.getSipTrunk(1)).thenReturn(Future.successful(Some(sip)))
      when(dbActions.getIaxTrunk(1)).thenReturn(Future.successful(Some(iax)))
      when(dbActions.getCustomTrunk(1)).thenReturn(Future.successful(Some(custom)))

      dbManagerActor ! GetTrunkInterface(routeTrunks, route)

      val expected = ResponseTrunkInterface(List(
        TrunkInterface(sip, "SIP/abcd", None),
        TrunkInterface(iax, "IAX/abcd", None),
        TrunkInterface(custom, "SIP/abcd", Some("suffix")),
        TrunkInterface(TrunkSip(1, s"mds0"), "SIP/to-mds0", None, TrunkOnAnotherMds)
      ), route)
      expectMsg(expected)
    }

    "not get any route" in new Helper {
      val ctx = ChannelData(Map(
        DESTINATION_NUMBER -> Some("1001"),
        MDS_NAME -> Some("mds0"),
        XIVO_BASE_CONTEXT -> Some("default")
      ))
      val mds = MediaServer(1, "mds0", "10.0.0.1")

      when(dbActions.getMediaserver("mds0")).thenReturn(Future.successful(Some(mds)))
      when(dbActions.getRouteByPattern("1001", "default", mds)).thenReturn(Future.successful(List()))

      dbManagerActor ! GetRoute(ctx)

      val expected = ResponseRoute(List())
      expectMsg(expected)
    }

    "not get any route if media server does not exist" in new Helper {
      val ctx = ChannelData(Map(
        DESTINATION_NUMBER -> Some("1001"),
        MDS_NAME -> Some("mds0"),
        XIVO_BASE_CONTEXT -> Some("default")
      ))

      when(dbActions.getMediaserver("mds0")).thenReturn(Future.successful(None))

      dbManagerActor ! GetRoute(ctx)

      val expected = ResponseRoute(List())
      expectMsg(expected)
    }
  }
}
