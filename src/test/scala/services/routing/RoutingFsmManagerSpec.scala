package services.routing

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import ari.actions.Ari
import models.events.ws.{StasisEnd, StasisStart}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus.AriWsMessage
import services.{AriEventBus, DatabaseManagers}
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

class RoutingFsmManagerSpec extends AnyWordSpecLike with Matchers with MockitoSugar with Eventually {

  implicit val system: ActorSystem = ActorSystem()

  class Helper() {
    val ariBus = mock[AriEventBus]
    val ari = mock[Ari]
    val dbActions = mock[RoutingDbActions]
    val helper = mock[RoutingHelper]
    val testprobe = TestProbe()

    val dbManagers = mock[DatabaseManagers]
    when(dbManagers.routeManager).thenReturn(testprobe.ref)

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val stasisStartEvent = StasisStart(List("1", "routePath"), channel, "08:00:27:80:c1:5f", "outcalls")
    val stasisEndEvent = StasisEnd(channel, "08:00:27:80:c1:5f", "outcalls")

    val managerActor = TestActorRef(new RoutingFsmManager(ariBus, helper, dbManagers))
    val manager: RoutingFsmManager = managerActor.underlyingActor
  }

  "RoutingFsmManager" should {
    "start and stop the fsm" in new Helper {
      managerActor ! AriWsMessage(stasisStartEvent)
      manager.initializedFsmIds.get("1549372028.17").isDefined shouldBe true

      managerActor ! AriWsMessage(stasisEndEvent)
      manager.initializedFsmIds.get("1549372028.17").isDefined shouldBe false
      manager.initializedFsmIds.get("1549372028.17").isEmpty shouldBe true

    }
  }
}
