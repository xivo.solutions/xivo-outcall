package ws

import org.apache.pekko.actor.{Actor, ActorRef, ActorSystem, Props}
import org.apache.pekko.pattern.BackoffSupervisor
import org.apache.pekko.testkit.{ImplicitSender, TestKit}
import app.Configuration
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.*
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.mockito.Mockito.{reset, timeout, verify, verifyNoInteractions, when}
import org.mockito.ArgumentMatchers.any

import scala.concurrent.duration.*

class WebsocketSupervisorSpec(_system: ActorSystem) extends TestKit(_system)
    with ImplicitSender with AnyWordSpecLike with Matchers with BeforeAndAfterAll with MockitoSugar with Eventually {

  class Helper {
    val config: Configuration = mock[Configuration]

    when(config.restartRetryMin).thenReturn(1.seconds)
    when(config.restartRetryMax).thenReturn(1.seconds)

    case object CrashItself
    class FailingActor extends Actor {
      override def receive = {
        case CrashItself => throw new Exception("Crashing itself")
      }
    }

    val wsSupervisor = system.actorOf(WebsocketSupervisor.initWebsocketActor(config, Props(new FailingActor)), name = "wsSupervisor")
  }

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
    _system.terminate()
  }

  "WebsocketSupervisor" should {
    "restart websocket actor" in new Helper {
      wsSupervisor ! BackoffSupervisor.GetCurrentChild
      val child: Option[ActorRef] = expectMsgType[BackoffSupervisor.CurrentChild].ref

      child.get ! CrashItself

      eventually(timeout(scaled(2500.milliseconds))) {
        wsSupervisor ! BackoffSupervisor.GetCurrentChild
        expectMsgType[BackoffSupervisor.CurrentChild].ref.get should !==(child)

        wsSupervisor ! BackoffSupervisor.GetRestartCount
        expectMsg(BackoffSupervisor.RestartCount(1))
      }

    }
  }
}
