package models

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.routing.RoutingDbActions.{TrunkCustom, TrunkIax, TrunkInterface, TrunkSip}

class ProtocolSpec extends AnyWordSpecLike with Matchers {
  class Helper() {
    val sip = TrunkSip(1, "xds-in-mds0")
    val iax = TrunkIax(1, "xds-in-mds0")
    val custom = TrunkCustom(1, "SIP/7c7d0cda", "suffix")
  }

  "Protocol" should {
    "build sip interfacce" in new Helper {
      Sip.interface(sip) shouldBe TrunkInterface(sip, "SIP/xds-in-mds0", None)
    }

    "build iax interfacce" in new Helper {
      Iax.interface(iax) shouldBe TrunkInterface(iax, "IAX/xds-in-mds0", None)
    }

    "build custom interfacce" in new Helper {
      Custom.interface(custom) shouldBe TrunkInterface(custom, "SIP/7c7d0cda", Some("suffix"))

    }
  }

}
