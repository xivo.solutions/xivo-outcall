package models.events

import models.events.ws.{StasisStart, WsEvents}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import play.api.libs.json._

class StasisStartSpec extends AnyWordSpecLike with Matchers {

  class Helper() {
    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")
  }

  "StasisStart" should {
    "be created from json" in new Helper {
      val statisStart = StasisStart(List(), channel, "08:00:27:80:c1:5f", "routePath")

      val json: JsValue = Json.parse("""{"type":"StasisStart","timestamp":"2019-02-05T14:07:09.004+0100","args":[],"channel":{"id":"1549372028.17","name":"SIP/mnwggob4-00000011","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-05T14:07:08.895+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"routePath"}""")

      json.validate[WsEvents] match {
        case JsSuccess(res, _) => res shouldEqual statisStart
        case JsError(errors) => fail()
      }
    }
  }
}
