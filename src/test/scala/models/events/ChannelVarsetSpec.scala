package models.events

import models.events.ws.{ChannelVarset, WsEvents}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import play.api.libs.json._

class ChannelVarsetSpec extends AnyWordSpecLike with Matchers {

  class Helper() {
    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549969544.11", "SIP/mnwggob4-0000000b", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")
  }

  "ChannelVarset" should {
    "be created from json" in new Helper {
      val channelVarSet = ChannelVarset("TEST", "12345", channel, "08:00:27:80:c1:5f", "outcalls")

      val json: JsValue = Json.parse("""{"variable":"TEST","value":"12345","type":"ChannelVarset","timestamp":"2019-02-05T14:07:08.895+0100","channel":{"id":"1549969544.11","name":"SIP/mnwggob4-0000000b","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-05T14:07:08.895+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"outcalls"}""")

      json.validate[WsEvents] match {
        case JsSuccess(res, _) => res shouldEqual channelVarSet
        case JsError(errors) => fail()
      }
    }
  }
}
