package models.events

import models.events.ws.{StasisEnd, WsEvents}
import models.{Caller, Channel, Connected, Dialplan}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import play.api.libs.json._

class StasisEndSpec extends AnyWordSpecLike with Matchers {

  class Helper() {
    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549537545.3", "SIP/mnwggob4-00000003", "Up", caller, connected, Some(""), dialplan, "2019-02-07T12:05:45.472+0100", "en_US")
  }

  "StasisEnd" should {
    "be created from json" in new Helper {
      val statisEnd = StasisEnd(channel, "08:00:27:80:c1:5f", "outcalls")

      val json: JsValue = Json.parse("""{"type":"StasisEnd","timestamp":"2019-02-07T12:05:48.506+0100","channel":{"id":"1549537545.3","name":"SIP/mnwggob4-00000003","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-07T12:05:45.472+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"outcalls"}""")

      json.validate[WsEvents] match {
        case JsSuccess(res, _) => res shouldEqual statisEnd
        case JsError(errors) => fail()
      }
    }
  }
}

