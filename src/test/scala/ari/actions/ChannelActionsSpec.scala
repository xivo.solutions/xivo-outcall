package ari.actions

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.TestProbe
import app.Configuration.ServiceId
import ari.actions.Channels.HangupReason
import models.events.bus._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus._

class ChannelActionsSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  implicit val system: ActorSystem = ActorSystem()

  class Helper() {
    val ariEventBus = new AriEventBus
    val appName = DefaultApp
    val testProbe = TestProbe()
    val fsmId: ServiceId = "123.45"

    val topic = AriTopic(topicType = TopicType.RESTREQUEST, appName = appName)
    val channels = new Channels(ariEventBus, appName)
  }

  "Channels" should {
    "publish GetChannels request" in new Helper {
      ariEventBus.subscribe(testProbe.ref, topic)
      channels.get(fsmId)

      val expected = AriRequestMessage(GetChannels, appName, "123.45")

      testProbe.expectMsg(expected)
    }

    "publish SetChannelVar request" in new Helper {
      ariEventBus.subscribe(testProbe.ref, topic)
      channels.setChannelVar("123.45", "variable", Some("value"), fsmId)

      val expected = AriRequestMessage(SetChannelVar("123.45", ChannelVar("variable", Some("value"))), DefaultApp, "123.45")

      testProbe.expectMsg(expected)
    }

    "publish HangupChannel request" in new Helper {
      ariEventBus.subscribe(testProbe.ref, topic)
      channels.hangup("123.45", Some(HangupReason.busy), fsmId)

      val expected = AriRequestMessage(HangupChannel("123.45", Some(HangupReason.busy)), DefaultApp, "123.45")

      testProbe.expectMsg(expected)
    }

    "publish HangupChannel without reason request" in new Helper {
      ariEventBus.subscribe(testProbe.ref, topic)
      channels.hangup("123.45", None, fsmId)

      val expected = AriRequestMessage(HangupChannel("123.45", None), DefaultApp, "123.45")

      testProbe.expectMsg(expected)
    }

    "publish ContinueInDialplan request" in new Helper {
      ariEventBus.subscribe(testProbe.ref, topic)
      channels.continue("123.45", None, None, None, None, fsmId)

      val expected = AriRequestMessage(ContinueInDialplan("123.45", None, None, None, None), DefaultApp, "123.45")

      testProbe.expectMsg(expected)
    }
  }
}

