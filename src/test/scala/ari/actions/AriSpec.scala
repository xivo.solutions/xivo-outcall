package ari.actions

import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus.DefaultApp

class AriSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  class Helper() {
    val ariEventBus = mock[AriEventBus]
    val appName = DefaultApp
  }

  "Ari" should {
    "initialize Channel actions" in new Helper {
      val actions = new Ari(ariEventBus, appName)
      actions.channels shouldBe a[Channels]
    }
  }
}
