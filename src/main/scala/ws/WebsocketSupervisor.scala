package ws

import org.apache.pekko.actor.{ OneForOneStrategy, Props, SupervisorStrategy }
import org.apache.pekko.pattern.{ BackoffOpts, BackoffSupervisor }
import app.Configuration
import com.typesafe.scalalogging.Logger

object WebsocketSupervisor {
  val logger: Logger = Logger(getClass.getName)

  def initWebsocketActor(config: Configuration, childProps: Props): Props = {

    val backoffOptions = BackoffOpts.onFailure(
      childProps,
      childName = "websocket",
      minBackoff = config.restartRetryMin,
      maxBackoff = config.restartRetryMax,
      randomFactor = 0.2
    ).withMaxNrOfRetries(-1)
      .withSupervisorStrategy(OneForOneStrategy(loggingEnabled = false) {
        case e: Exception =>
          logger.error(s"Websocket is being restarted due to ${e.getMessage}")
          SupervisorStrategy.Restart
      })
    BackoffSupervisor.props(backoffOptions)
  }
}
