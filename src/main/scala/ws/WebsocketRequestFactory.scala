package ws

import org.apache.pekko.Done
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.model.ws.{ Message, WebSocketRequest, WebSocketUpgradeResponse }
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.stream.{ KillSwitches, SharedKillSwitch }
import app.Configuration
import services.AriEventBus.OutcallApp

import scala.concurrent.{ Future, Promise }

trait WebsocketRequestFactory {
  implicit def appToString(app: OutcallApp): String = app.toString

  def getWsRequest(appName: OutcallApp, wsFlow: Flow[Message, Message, (Future[Done], Promise[Option[Message]])]): (Future[WebSocketUpgradeResponse], (Future[Done], Promise[Option[Message]]))

  val sharedKillSwitch: SharedKillSwitch = KillSwitches.shared("ws-shutdown")
}

class ProductionWebsocketRequestFactory(config: Configuration)(implicit system: ActorSystem)
    extends WebsocketRequestFactory {

  def createWsUri(appName: String): String = {
    s"ws://${config.ariHost}:${config.ariPort}/ari/events?api_key=${config.ariUsername}:${config.ariPassword}&app=$appName"
  }

  def getWsRequest(appName: OutcallApp, wsFlow: Flow[Message, Message, (Future[Done], Promise[Option[Message]])]): (Future[WebSocketUpgradeResponse], (Future[Done], Promise[Option[Message]])) = {
    Http().singleWebSocketRequest(WebSocketRequest(s"${createWsUri(appName)}"), wsFlow.via(sharedKillSwitch.flow))
  }
}
