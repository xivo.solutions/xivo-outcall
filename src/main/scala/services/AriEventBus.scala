package services

import org.apache.pekko.event.{ ActorEventBus, LookupClassification }
import app.Configuration.ServiceId
import models.DialplanObjects
import models.events.bus.BusEvents
import models.events.ws.WsEvents
import services.AriEventBus.TopicType.TopicType
import services.AriEventBus.{ AriEvent, AriTopic }

object AriEventBus {
  object TopicType extends Enumeration {
    type TopicType = Value
    val WSEVENT, RESTRESPONSE, RESTREQUEST = Value
  }

  trait OutcallApp
  case object ScheduleApp extends OutcallApp {
    override def toString = "schedules"
  }
  case object RightsApp extends OutcallApp {
    override def toString = "rights"
  }
  case object RoutingApp extends OutcallApp {
    override def toString = "routing"
  }
  case object InitChannelVarsApp extends OutcallApp {
    override def toString = "initchannelvars"
  }
  case object DefaultApp extends OutcallApp {
    override def toString = "outcalls"
  }

  sealed trait AriMessage
  case class AriRequestMessage(resource: BusEvents, app: OutcallApp, serviceId: ServiceId) extends AriMessage
  case class AriResponseMessage(answer: DialplanObjects, serviceId: ServiceId) extends AriMessage
  case class AriWsMessage(message: WsEvents) extends AriMessage

  case class AriTopic(topicType: TopicType, appName: OutcallApp)
  case class AriEvent(topic: AriTopic, message: AriMessage)
}

class AriEventBus extends ActorEventBus with LookupClassification {
  type Event = AriEvent
  type Classifier = AriTopic

  override def publish(event: Event): Unit = super.publish(event)

  override def subscribe(subscriber: Subscriber, to: Classifier): Boolean =
    super.subscribe(subscriber, to)

  override def unsubscribe(subscriber: Subscriber, from: Classifier): Boolean =
    super.unsubscribe(subscriber, from)

  override def unsubscribe(subscriber: Subscriber): Unit =
    super.unsubscribe(subscriber)

  override protected def classify(event: Event): Classifier = event.topic

  override protected def publish(event: Event, subscriber: Subscriber): Unit = {
    subscriber ! event.message
  }

  override protected def compareSubscribers(a: Subscriber, b: Subscriber): Int =
    a.compareTo(b)

  override protected def mapSize(): Int = 128
}