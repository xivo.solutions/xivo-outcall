package services.routing

import org.apache.pekko.actor.{Actor, ActorSystem, Props}
import org.apache.pekko.pattern.pipe
import app.helpers.LogWithChannel
import models._
import services.routing.RoutingDbActions._
import services.routing.RoutingDbManager._

import scala.concurrent.{ExecutionContext, Future}

object RoutingDbManager {
  sealed trait GenericResponse
  case class Response[T](results: List[T]) extends GenericResponse
  case class ResponseRoute(routes: List[Route]) extends GenericResponse
  case class ResponseRouteTrunk(routeTrunks: Map[Route, List[RouteTrunk]]) extends GenericResponse
  case class ResponseTrunkInterface(interfaces: List[TrunkInterface], route: Route) extends GenericResponse

  def props(dbActions: RoutingDbActions, helper: RoutingHelper)(implicit system: ActorSystem): Props = {
    Props(new RoutingDbManager(dbActions, helper))
  }
}

class RoutingDbManager(dbActions: RoutingDbActions, helper: RoutingHelper) extends Actor with LogWithChannel {
  implicit val ec: ExecutionContext = context.dispatcher

  override def preStart(): Unit = {
    logger.info(s"$self starting")
  }

  override def postStop(): Unit = {
    logger.info(s"$self stop")
  }

  override def receive: Receive = {
    case GetRoute(channelData) =>
      helper.getDataForRoute(channelData).map { d =>
        dbActions.getMediaserver(d.mdsName).flatMap {
          case Some(m) => dbActions.getRouteByPattern(d.destination, d.context, m)
          case None => Future.successful(List())
        }
      }.map(rf => rf.map(r => ResponseRoute(r))
        .pipeTo(sender()))

    case GetRouteTrunk(routes) =>
      Future.sequence(getRouteTrunk(routes).map { case (key, futureValue) => futureValue.map(key -> _) })
        .map(rt => ResponseRouteTrunk(rt.toMap))
        .pipeTo(sender())

    case GetTrunkInterface(trunkList, route) =>
      Future.sequence(trunkList.map(t => buildInterface(t.protocolId, t.protocol, t.trunkLocation, t.mediaserverName)))
        .map(i => ResponseTrunkInterface(i.flatten, route))
        .pipeTo(sender())

    case other => logger.error(s"Received an unexpected database request $other")
  }

  def buildInterface(protocolId: Int, protocol: Protocol, trunkLocation: TrunkLocation, mdsName: String): Future[Option[RoutingDbActions.TrunkInterface]] = {
    getTrunkByProtocol(protocol, protocolId, mdsName).map { maybeTrunkInterface =>
      maybeTrunkInterface.map { trunkInterface =>
        trunkLocation match {
          case TrunkOnAnotherMds => Mds.interface(TrunkSip(protocolId, mdsName))
          case TrunkOnCurrentMds | TrunkUnknownMds => trunkInterface
        }
      }
    }
  }

  private def getTrunkByProtocol(protocol: Protocol, protocolId: Int, mdsName: String): Future[Option[TrunkInterface]] = {
    protocol match {
      case Sip => dbActions.getSipTrunk(protocolId).map(t => t.map(Sip.interface))
      case Iax => dbActions.getIaxTrunk(protocolId).map(t => t.map(Iax.interface))
      case Custom => dbActions.getCustomTrunk(protocolId).map(t => t.map(Custom.interface))
      case Mds => Future.successful(Some(Mds.interface(TrunkSip(protocolId, mdsName))))
    }
  }

  private def getRouteTrunk(routes: List[Route]): Map[Route, Future[List[RouteTrunk]]] = {
    routes.map(r => r -> dbActions.getRouteTrunk(r.id)).toMap
  }
}
