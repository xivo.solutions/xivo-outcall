package services.routing

import org.apache.pekko.actor.{ ActorSystem, Props }
import ari.actions.Ari
import models.ChannelVariables._
import models._
import models.events.ws.{ ChannelVarset, StasisStart }
import services.FsmFactory.{ FsmContext, Initial, Uninitialized }
import services._
import services.routing.RoutingDbActions.{ Route, RouteTrunk, TrunkOnAnotherMds, TrunkOnCurrentMds }
import services.routing.RoutingDbManager._
import services.routing.RoutingFsm._

import scala.concurrent.duration._

object RoutingFsm {
  case object AwaitRoute extends FsmState
  case object AwaitRouteTrunk extends FsmState
  case object AwaitSetTrunkInDialplan extends FsmState

  case object AwaitInterface extends FsmState
  case object AwaitChannelVars extends FsmState

  sealed trait ChannelVariablesSet {
    val name: String
  }
  case class SetTrunkSuffix(name: String) extends ChannelVariablesSet
  case class SetTrunkInterface(name: String) extends ChannelVariablesSet
  case class SetTrunkExten(name: String, extension: String) extends ChannelVariablesSet
  case class SetTrunkGotoMds(name: String) extends ChannelVariablesSet

  def props(ariEventBus: AriEventBus, ariImp: Ari, databaseManagers: DatabaseManagers,
    channelImpl: Channel, helper: RoutingHelper)(implicit system: ActorSystem): Props = {
    Props(new RoutingFsm(ariImp, databaseManagers, helper, channelImpl))
  }
}

class RoutingFsm(ariImp: Ari, databaseManagers: DatabaseManagers, helper: RoutingHelper, channelImpl: Channel)
    extends FsmFactory {

  val mdsName: String = sys.env.getOrElse(MDS_NAME, "default")

  override val ari: Ari = ariImp
  override val channel: Channel = channelImpl
  override val dbManagers: DatabaseManagers = databaseManagers

  startWith(Initial, Uninitialized)

  when(Initial) {
    case Event(e: StasisStart, Uninitialized) =>
      val awaitChannelVars = List(DESTINATION_NUMBER, XIVO_BASE_CONTEXT)
      val channelData = ChannelData(Map()).addChannelVar(MDS_NAME, Some(mdsName))
      awaitChannelVars.foreach(v => ari.channels.getChannelVar(e.channel.id, v, channel.id))
      chanLog.info(e.channel, "entering call routing")
      goto(AwaitChannelVars) using FsmContext(awaitChannelVars, e.channel, channelData)
  }

  when(AwaitChannelVars, stateTimeout = 3.seconds) {
    case Event(e: ChannelVarResponse, stateContext: FsmContext) =>
      val newCtx = FsmContext(helper.removeOneAwaited(e, stateContext), stateContext.channel,
        helper.updateChannelData(e, stateContext.channelData))
      newCtx.waitOrGotoState(AwaitRoute, GetRoute(newCtx.channelData))
  }

  when(AwaitRoute, stateTimeout = 3.seconds) {
    case Event(t: ResponseRoute, stateContext: FsmContext) =>
      chanLog.info(channel, s"found available routes ${t.routes}")
      stateContext.waitOrGotoState(AwaitRouteTrunk, GetRouteTrunk(t.routes))
  }

  when(AwaitRouteTrunk, stateTimeout = 3.seconds) {
    case Event(t: ResponseRouteTrunk, stateContext: FsmContext) =>
      val firstRouteWithTrunks = t.routeTrunks.headOption
      firstRouteWithTrunks match {
        case Some(routeWithTrunks) => collectTrunks(stateContext, mdsName, routeWithTrunks)
        case None => errorHandling("the route does not exist")
      }
  }

  when(AwaitInterface, stateTimeout = 3.seconds) {
    case Event(response: ResponseTrunkInterface, stateContext: FsmContext) =>
      response.interfaces match {
        case Nil => errorHandling(s"no enabled trunks found in route ${response.route}")
        case trunkInterfaces =>
          chanLog.info(channel, s"routing to trunks ${trunkInterfaces.map(_.interface)}")
          val setChannelVars = helper.processTrunkInterfaces(channel, stateContext.channelData.getChannelVar(DESTINATION_NUMBER).get, trunkInterfaces, response.route)
          val newContext = stateContext.copy(awaiting = setChannelVars.map(_.name))
          goto(AwaitSetTrunkInDialplan) using newContext
      }
  }

  when(AwaitSetTrunkInDialplan, stateTimeout = 3.seconds) {
    case Event(e: ChannelVarset, stateContext: FsmContext) =>
      val newCtx = stateContext.copy(awaiting = helper.removeOneAwaited(e, stateContext))
      newCtx.awaitAndStop
  }

  private def collectTrunks(context: FsmContext, currentMds: String, routeWithTrunks: (Route, List[RouteTrunk])): State = {
    val trunkList = routeWithTrunks._2.map {
      case t if currentMds != t.mediaserverName => t.copy(trunkLocation = TrunkOnAnotherMds)
      case t => t.copy(trunkLocation = TrunkOnCurrentMds)
    }
    if (trunkList.nonEmpty) {
      chanLog.info(channel, s"found available trunks $trunkList")
      context.waitOrGotoState(AwaitInterface, GetTrunkInterface(trunkList, routeWithTrunks._1))
    } else {
      errorHandling(s"no trunks found in route $routeWithTrunks")
    }
  }
}
