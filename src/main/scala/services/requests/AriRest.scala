package services.requests

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.model.headers.Authorization
import org.apache.pekko.http.scaladsl.model.{ HttpMethod, HttpRequest, HttpResponse }

import scala.concurrent.Future

class AriRest(implicit system: ActorSystem) {

  def singleRequest(uri: String, method: HttpMethod, authHeaders: Authorization): Future[HttpResponse] = {
    Http().singleRequest(
      HttpRequest(uri = uri)
        .withMethod(method)
        .withHeaders(authHeaders)
    )
  }
}
