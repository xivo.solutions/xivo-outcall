package services.requests

import org.apache.pekko.actor.{Actor, ActorSystem}
import org.apache.pekko.http.scaladsl.model.*
import org.apache.pekko.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import org.apache.pekko.http.scaladsl.unmarshalling.{PredefinedFromEntityUnmarshallers, Unmarshal, Unmarshaller}
import app.Configuration
import com.typesafe.scalalogging.LazyLogging
import models.Channel.CurrentChannels
import models.ChannelVarResponse
import models.events.bus.*
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}
import services.AriEventBus
import services.AriEventBus.*

import scala.concurrent.{ExecutionContext, Future}

class Requester(config: Configuration, ariRest: AriRest, ariEventBus: AriEventBus)(implicit system: ActorSystem, ec: ExecutionContext) extends Actor with LazyLogging {
  val authorization: Authorization = headers.Authorization(BasicHttpCredentials(config.ariUsername, config.ariPassword))

  override def preStart(): Unit = {
    logger.info(s"$self starting")
    ariEventBus.subscribe(self, AriTopic(topicType = TopicType.RESTREQUEST, appName = DefaultApp))
    context.become(receive)
  }

  override def postStop(): Unit = {
    logger.info(s"$self stop")
    ariEventBus.unsubscribe(self)
  }

  override def receive = {
    case msg: AriRequestMessage =>
      logger.debug(s"Received bus event $msg")
      msg.resource match {
        case GetChannels =>
          executeWithResponse[CurrentChannels](GetChannels.resource, HttpMethods.GET, request).map { res =>
            res.foreach(c => ariEventBus.publish(AriEvent(AriTopic(TopicType.RESTRESPONSE, msg.app), AriResponseMessage(c, msg.serviceId))))
          }
        case c: ContinueInDialplan => execute(c.resource, HttpMethods.POST, request)
        case h: HangupChannel => execute(h.resource, HttpMethods.DELETE, request)
        case c: SetChannelVar => execute(c.resource, HttpMethods.POST, request)
        case c: GetChannelVar => executeWithResponse[models.ChannelVar](c.resource, HttpMethods.GET, request).map(res =>
          ariEventBus.publish(AriEvent(AriTopic(TopicType.RESTRESPONSE, msg.app), AriResponseMessage(ChannelVarResponse(c.channelVar, res), msg.serviceId))))
        case other => logger.warn(s"Unsupported action $other")
      }
    case other => logger.error(s"Received unknown bus event $other")
  }

  def createUri(resource: String): String = s"http://${config.ariHost}:${config.ariPort}/ari/$resource"

  def processResponse[T](response: HttpResponse)(implicit rds: Reads[T]): Future[T] = {
    implicit val unm: Unmarshaller[HttpEntity, T] = {
      PredefinedFromEntityUnmarshallers.stringUnmarshaller.map { data =>
        Json.parse(data).validate[T] match {
          case e: JsError =>
            logger.error(s"Non understandable response returned : $e")
            throw new Exception("Non understandable response returned")
          case res: JsSuccess[T] => res.get
        }
      }
    }
    Unmarshal(response.entity).to[T]
  }

  def request(resource: String, method: HttpMethod): Future[HttpResponse] = {
    ariRest.singleRequest(createUri(resource), method, authorization)
  }

  case class AriError(error: String)
  private def executeWithResponse[T](resource: String, method: HttpMethod, httpRequest: (String, HttpMethod) => Future[HttpResponse])(implicit rds: Reads[T]): Future[T] = {
    httpRequest(resource, method).flatMap {
      case response @ HttpResponse(StatusCodes.OK, headers, entity, _) =>
        processResponse[T](response)
      case response @ HttpResponse(StatusCodes.NotFound, headers, entity, _) =>
        processResponse[T](response)
      case e =>
        logger.error(s"Request failed $e")
        throw new Exception(s"Request failed, $e")
    }
  }

  private def execute[T](resource: String, method: HttpMethod, httpRequest: (String, HttpMethod) => Future[HttpResponse]): Future[Unit] = {
    httpRequest(resource, method).map {
      case response @ HttpResponse(StatusCodes.NoContent, _, _, _) =>
      case e =>
        logger.error(s"Request failed $e")
        throw new Exception(s"Request failed $e")
    }
  }
}
