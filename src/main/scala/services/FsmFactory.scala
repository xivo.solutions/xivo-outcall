package services

import java.sql.SQLException
import org.apache.pekko.actor.FSM
import org.apache.pekko.actor.Status.Failure
import app.helpers.LogWithChannel
import ari.actions.Ari
import models.ChannelVariables.{FORWARDERNAME, FWD_REFERER, XIVO_OUTCALL_ERROR}
import models._
import models.events.ws.ChannelVarset

import scala.concurrent.Future

trait FsmState

trait FsmData

trait Context extends FsmData {
  val awaiting: List[String]
  val channel: Channel
  val channelData: ChannelData
}

object FsmFactory {

  case object Initial extends FsmState

  case object Uninitialized extends FsmData

  case class FsmContext(awaiting: List[String], channel: Channel, channelData: ChannelData) extends Context

}

trait FsmFactory extends LogWithChannel with FSM[FsmState, FsmData] {
  val ari: Ari
  val channel: Channel
  val dbManagers: DatabaseManagers

  override def preStart(): Unit = {
    chanLog.info(channel, s"$self starting")
  }

  override def postStop(): Unit = {
    chanLog.info(channel, s"$self stop")
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    errorHandling(reason.toString)
  }

  whenUnhandled {
    case Event(StateTimeout, _: Context) =>
      errorHandling("state timeout")
    case Event(Failure(f: Throwable), _: Context) =>
      errorHandling(s"${f.getMessage}", Some(f))
    case Event(_: ChannelVarset, _) =>
      stay()
    case Event(e, _: Context) =>
      errorHandling(s"received unhandled request $e")
    case Event(e, d) =>
      errorHandling(s"received unhandled request $e and unknown context data in state $stateName. Context data dump: $d")
  }

  onTermination {
    case StopEvent(FSM.Shutdown, state, data) =>
      setErrorAndContinueInDialplan("FSM Shutdown, make the current call fail.")
    case StopEvent(FSM.Failure(cause), state, data) =>
      chanLog.error(channel, s"FSM Failed because of $cause")
  }

  private def setErrorAndContinueInDialplan(msg: String): Unit = {
    chanLog.error(channel, s"$msg.\nContext data dump: $stateData")
    ari.channels.setChannelVar(channel.id, XIVO_OUTCALL_ERROR, Some(msg), channel.id)
    ari.channels.continue(channel.id, None, Some("failed"), Some(1), None, channel.id)
  }

  def errorHandling(errorMessage: String, e: Option[Throwable] = None): State = {
    val msg = s"state $stateName failed with error: $errorMessage"
    setErrorAndContinueInDialplan(msg)
    e match {
      case Some(psqle: SQLException) =>
        stop(FSM.Failure(psqle))
      case Some(any) =>
        stop()
      case None =>
        stop()
    }
  }

  def getForwarderUserIdIfExists(forwardType: ForwardType, fsmContext: Context, getIdFromChannel: String => Future[Forwarded]): Future[Forwarded] = {
    forwardType match {
      case ForwardXivo => fsmContext.channelData.getChannelVar(FWD_REFERER) match {
        case Some(referer) =>
          if (referer.startsWith("user") && referer.split(":").length > 1) {
            Future.successful(ForwardedUser(referer.split(":")(1)))
          } else
            Future.successful(ForwardedElseWhere)
        case None => Future.successful(ForwardedElseWhere)
      }
      case ForwardSip =>
        fsmContext.channelData.getChannelVar(FORWARDERNAME) match {
          case Some(forwarder) => getIdFromChannel(forwarder)
          case None => Future.successful(ForwardedElseWhere)
        }
    }
  }

  implicit class ToState(context: FsmData) {
    def waitOrGotoState(state: FsmState, request: DatabaseQuery): State = {
      context match {
        case c: Context =>
          if (c.awaiting.nonEmpty) {
            stay() using context
          } else {
            request match {
              case r: InitChannelVarsQuery => dbManagers.initChannelVarsManager ! r
              case r: RightsDbQuery => dbManagers.rightsManager ! r
              case r: RoutingDbQuery => dbManagers.routeManager ! r
            }
            goto(state) using context
          }
        case _ =>
          chanLog.error(channel, s"unknown context $context, should never get here")
          goto(state) using context
      }
    }

    def awaitAndStop: State = {
      context match {
        case c: Context =>
          if (c.awaiting.nonEmpty) {
            stay() using context
          } else {
            ari.channels.continue(channel.id, None, None, None, None, channel.id)
            stop()
          }
        case _ =>
          chanLog.error(channel, s"unknown context $context, should never get here")
          ari.channels.continue(channel.id, None, None, None, None, channel.id)
          stop()
      }
    }

    def awaitAndStopWithExecute[T](action: => T): State = {
      context match {
        case c: Context =>
          if (c.awaiting.nonEmpty) {
            stay() using context
          } else {
            ari.channels.continue(channel.id, None, None, None, None, channel.id)
            action
            stop()
          }
        case _ =>
          chanLog.error(channel, s"unknown context $context, should never get here")
          ari.channels.continue(channel.id, None, None, None, None, channel.id)
          stop()
      }
    }
  }

}
