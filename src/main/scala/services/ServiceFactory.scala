package services

import org.apache.pekko.actor.{ ActorSystem, Props }
import app.Configuration
import models.{ InitChannelVarsFsmManagerService, RightsFsmManagerService, RoutingFsmManagerService, ServiceManager }
import services.initChannelVars.{ InitChannelFsmManager, InitChannelVarsHelper }
import services.rights.{ RightsDbActions, RightsFsmManager, RightsHelper }
import services.routing.{ RoutingDbActions, RoutingFsmManager, RoutingHelper }

trait ServiceFactory {
  def props(s: ServiceManager): Props
}

class ServiceFactoryImpl(config: Configuration, ariEventBus: AriEventBus,
    rightsDb: RightsDbActions, rightsHelper: RightsHelper,
    routeDb: RoutingDbActions, routeHelper: RoutingHelper,
    initChannelHelper: InitChannelVarsHelper, databaseManagers: DatabaseManagers)(implicit system: ActorSystem) extends ServiceFactory {

  override def props(s: ServiceManager): Props = s match {
    case RightsFsmManagerService => Props(new RightsFsmManager(ariEventBus, rightsHelper, rightsDb, databaseManagers))
    case RoutingFsmManagerService => Props(new RoutingFsmManager(ariEventBus, routeHelper, databaseManagers))
    case InitChannelVarsFsmManagerService => Props(new InitChannelFsmManager(ariEventBus, rightsDb, rightsHelper, routeDb, routeHelper, initChannelHelper, databaseManagers))
  }
}
