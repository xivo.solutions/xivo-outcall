package services

import org.apache.pekko.actor.{ Actor, ActorRef, ActorSystem, Props }
import app.Configuration.ServiceId
import app.helpers.LogWithChannel
import ari.actions.Ari
import models.Channel
import models.ChannelVariables.XIVO_OUTCALL_ERROR
import services.AriEventBus._

import scala.collection.immutable.HashMap

trait ServiceManagerFactory extends Actor with LogWithChannel {

  val appType: OutcallApp
  val ariBus: AriEventBus
  val ari: Ari
  def props(channel: Channel): Props
  var initializedFsmIds: HashMap[ServiceId, ActorRef] = HashMap()

  def handleAriWsMessage(msg: AriWsMessage): Unit

  def handleAriResponseMessage(msg: AriResponseMessage): Unit = {
    logger.debug(s"Received rest event $msg")
    getFsmForChannel(msg.serviceId) match {
      case Some(r) => r ! msg.answer
      case None =>
        val channelId = msg.serviceId
        ari.channels.setChannelVar(channelId, XIVO_OUTCALL_ERROR, Some(s"Received response ${msg.answer} for uninitialized service $channelId"), channelId)
        ari.channels.continue(channelId, None, Some("failed"), Some(1), None, channelId)
    }
  }

  override def preStart(): Unit = {
    logger.info(s"$self starting")
    subscribe(appType)
  }

  override def postStop(): Unit = {
    logger.debug(s"$self stop")
    unsubscribe()
  }

  override def receive: Receive = {
    case msg: AriWsMessage => handleAriWsMessage(msg)
    case msg: AriResponseMessage => handleAriResponseMessage(msg)
    case other => logger.error(s"Should not get message here $other")
  }

  private def subscribe(appType: OutcallApp) = {
    ariBus.subscribe(self, AriTopic(topicType = TopicType.WSEVENT, appType))
    ariBus.subscribe(self, AriTopic(topicType = TopicType.RESTRESPONSE, appType))
  }

  private def unsubscribe(): Unit = ariBus.unsubscribe(self)

  def initializeNewFsm(channel: Channel)(implicit system: ActorSystem): ActorRef = {
    val serviceId = channel.id
    val newScheduleService: ActorRef = system.actorOf(props(channel))
    initializedFsmIds += serviceId -> newScheduleService
    newScheduleService
  }

  def getFsmForChannel(serviceId: ServiceId): Option[ActorRef] = {
    initializedFsmIds.get(serviceId) match {
      case Some(ref) => Some(ref)
      case None =>
        logger.error(s"Received response for uninitialized service $serviceId")
        None
    }
  }
}
