package services

import org.apache.pekko.actor.{ ActorRef, ActorSystem }
import services.initChannelVars.InitChannelVarsDbManager
import services.rights.{ RightsDbActions, RightsDbManager, RightsHelper }
import services.routing.{ RoutingDbActions, RoutingDbManager, RoutingHelper }

class DatabaseManagers(rightsDb: RightsDbActions, rightsHelper: RightsHelper, routingDb: RoutingDbActions, routingHelper: RoutingHelper)(implicit system: ActorSystem) {
  val rightsManager: ActorRef = system.actorOf(RightsDbManager.props(rightsDb, rightsHelper), "rightsDbManager")
  val routeManager: ActorRef = system.actorOf(RoutingDbManager.props(routingDb, routingHelper), "routingDbManager")
  val initChannelVarsManager: ActorRef = system.actorOf(InitChannelVarsDbManager.props(rightsHelper), "initChannelDbManager")
}
