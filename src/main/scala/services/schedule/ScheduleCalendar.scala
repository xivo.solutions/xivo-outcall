package services.schedule

import java.time.{Clock, LocalDate}
import java.util
import app.helpers.LogWithChannel
import models.Channel
import services.schedule.ScheduleCalendar._
import services.schedule.ScheduleDbManager.{ModeClosed, ModeOpened, PeriodMode, UncheckedSchedulePeriods}
import services.schedule.ScheduleService.RoutePath

import scala.concurrent.{ExecutionContext, Future}

object ScheduleCalendar {
  type ScheduleName = String

  sealed trait ScheduleAction
  case object ScheduleActionNone extends ScheduleAction
  case class EndcallHangup(action: String, actionId: Option[String], actionArgs: Option[String]) extends ScheduleAction
  case class ProcessInDialplan(action: String, actionId: Option[String], actionArgs: Option[String]) extends ScheduleAction

  case class ScheduleHours(start: String, end: String)
  case class SchedulePeriod(year: Int, month: Int, dayOfMonth: Int)
  case class DateSchedulePeriods(periods: List[LocalDate], weekdays: List[Int], action: ScheduleAction)

  case class CheckedSchedulePeriods(calendar: util.List[LocalDate], action: ScheduleAction)
  case class Schedule(state: ScheduleState, action: ScheduleAction, scheduleName: Option[String] = None)

  sealed trait ScheduleState
  case object ScheduleStateOpened extends ScheduleState {
    override def toString: String = "OPENED"
  }
  case object ScheduleStateClosed extends ScheduleState {
    override def toString: String = "CLOSED"
  }
}

class ScheduleCalendar(scheduleManagerDb: ScheduleDbManager, clock: Clock)(implicit ec: ExecutionContext) extends LogWithChannel {

  private[schedule] val parser = new ScheduleParser(clock)
  private[schedule] var defaultAction: ScheduleAction = ScheduleActionNone

  def processSchedule(routePath: Option[RoutePath], channel: Channel): Future[Schedule] = {
    def processPath(outPath: RoutePath): Future[Schedule] = {
      scheduleManagerDb.getSchedulePath(outPath).flatMap { schedulePath =>
        schedulePath.map{ path =>
          defaultAction = path.fallback_action

          chanLog.info(channel, s"schedule ${path.scheduleName} found for route id ${outPath.pathId}")
          scheduleManagerDb.getSchedulePeriods(path).map{ uncheckedSchedulePeriods =>
            val uncheckedClosedSchedule = getUncheckedSchedule(uncheckedSchedulePeriods, ModeClosed)
            val uncheckedOpenedSchedule = getUncheckedSchedule(uncheckedSchedulePeriods, ModeOpened)

            val checkedClosedSchedule = getSchedule(uncheckedClosedSchedule, ScheduleStateClosed, path.scheduleName)
            val checkedOpenedSchedule = getSchedule(uncheckedOpenedSchedule, ScheduleStateOpened, path.scheduleName)

            scheduleDecider(checkedClosedSchedule, checkedOpenedSchedule, path.scheduleName)
          }
        }.getOrElse{
          chanLog.info(channel, s"no schedule found for route ${outPath.pathId}")
          Future.successful(Schedule(ScheduleStateOpened, defaultAction))
        }
      }
    }
    routePath
      .map(processPath)
      .getOrElse(Future.successful(Schedule(ScheduleStateOpened, defaultAction)))
  }

  private def scheduleDecider(closed: Option[Schedule], opened: Option[Schedule], scheduleName: String) = {
    (closed, opened) match {
      case (Some(closedSchedule), Some(_)) => closedSchedule
      case (None, Some(openedSchedule)) => openedSchedule
      case (Some(closedSchedule), None) => closedSchedule
      case (_, _) => Schedule(ScheduleStateClosed, defaultAction, Some(scheduleName))
      case _ => Schedule(ScheduleStateOpened, ScheduleActionNone)
    }
  }

  private def getUncheckedSchedule(schedulePeriods: List[UncheckedSchedulePeriods], mode: PeriodMode) = {
    schedulePeriods.filter(period => period.mode == mode)
      .filter(p => parser.splitToHour(p.hours).isDefined)
  }

  private def getSchedule(uncheckedSchedule: List[UncheckedSchedulePeriods], scheduleType: ScheduleState, scheduleName: ScheduleName) = {
    uncheckedSchedule
      .takeWhile(hoursWithinInterval)
      .map(createPeriods)
      .map(createScheduleCalendar)
      .filterNot(c => c.calendar.isEmpty)
      .map(createSchedule(scheduleType, _, scheduleName)).lastOption
  }

  private def hoursWithinInterval(uncheckedPeriods: UncheckedSchedulePeriods) =
    parser.isHoursWithinInterval(parser.splitToHour(uncheckedPeriods.hours))

  private def createPeriods(schedulePeriods: UncheckedSchedulePeriods) =
    parser.createPeriods(parser.splitMonths(schedulePeriods.months), parser.splitMonthDays(schedulePeriods.monthdays),
      parser.splitWeekDays(schedulePeriods.weekdays), setDialplanAction(schedulePeriods.action))

  private def createScheduleCalendar(period: DateSchedulePeriods) = parser.createScheduleCalendars(period)

  private def createSchedule(scheduleType: ScheduleState, checkedPeriods: CheckedSchedulePeriods, name: ScheduleName) = Schedule(scheduleType, checkedPeriods.action, Some(name))

  private def setDialplanAction(action: ScheduleAction): ScheduleAction = {
    action match {
      case ScheduleActionNone => defaultAction
      case _ => action
    }
  }
}
