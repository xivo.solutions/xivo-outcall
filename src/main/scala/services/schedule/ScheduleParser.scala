package services.schedule

import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.time.{ Clock, LocalDate, LocalTime }
import java.util
import java.util.stream.{ Collectors, IntStream }

import services.schedule.ScheduleCalendar._

import scala.util.Try

class ScheduleParser(clock: Clock) {
  final val formatterHours = DateTimeFormatter.ofPattern("H[H]:mm[:ss][.SSS]")

  def createPeriods(months: List[Int], monthDays: List[Int], weekDays: List[Int], action: ScheduleAction): DateSchedulePeriods = {
    val schedulePeriod: List[SchedulePeriod] = for (
      i <- months;
      j <- monthDays
    ) yield SchedulePeriod(LocalDate.now(clock).getYear, i, j)
    val schedulePeriods: List[LocalDate] = schedulePeriod.flatMap(p => Try(LocalDate.of(p.year, p.month, p.dayOfMonth)).toOption)
    DateSchedulePeriods(schedulePeriods, weekDays, action)
  }

  def createScheduleCalendars(schedulePeriods: DateSchedulePeriods): CheckedSchedulePeriods = {
    val currentYear = LocalDate.now(clock).getYear

    val daysBetween = ChronoUnit.DAYS.between(
      LocalDate.of(currentYear, 1, 1),
      LocalDate.of(currentYear, 12, 31)
    )

    val scheduleCalendar: util.List[LocalDate] = IntStream.iterate(0, i => i + 1)
      .limit(daysBetween)
      .mapToObj(i => LocalDate.of(currentYear, 1, 1).plusDays(i))
      .filter((t: LocalDate) => schedulePeriods.weekdays.contains(t.getDayOfWeek.getValue))
      .filter((t: LocalDate) => schedulePeriods.periods.contains(t))
      .filter((t: LocalDate) => t.isEqual(LocalDate.now(clock)))
      .collect(Collectors.toList())
    CheckedSchedulePeriods(scheduleCalendar, schedulePeriods.action)
  }

  private def createInterval(interval: Option[String]): List[Int] = {
    interval match {
      case Some(i) => i.split(",").toList.flatMap { w =>
        if (w.contains("-")) {
          val splitted = w.split("-")
          (splitted(0).toInt to splitted(1).toInt).toList
        } else List(w.toInt)
      }
      case None => List()
    }
  }

  def isHoursWithinInterval(hours: Option[ScheduleHours]): Boolean = {
    val start = LocalTime.parse(hours.get.start, formatterHours)
    val stop = LocalTime.parse(hours.get.end, formatterHours)

    LocalTime.now(clock).isAfter(start) && LocalTime.now(clock).isBefore(stop)
  }

  def splitToHour(hours: String): Option[ScheduleHours] = {
    hours.split("-") match {
      case Array(start, end) => Some(ScheduleHours(start, end))
      case _ => None
    }
  }

  def splitWeekDays(weekdays: Option[String]) = createInterval(weekdays)

  def splitMonthDays(dates: Option[String]) = createInterval(dates)

  def splitMonths(months: Option[String]) = createInterval(months)

}
