package services.schedule

import org.apache.pekko.actor.{Actor, Status}
import app.helpers.LogWithChannel
import ari.actions.Ari
import models.Channel
import models.ChannelVariables.XIVO_OUTCALL_ERROR
import models.events.ws.{ChannelVarset, StasisStart}
import services.AriEventBus
import services.schedule.ScheduleService._
import services.schedule.ScheduleServiceManager.ContinueInDialplan

import scala.concurrent.ExecutionContext

object ScheduleService {
  case class RoutePath(path: String, pathId: Int)
}

class ScheduleService(ariEventBus: AriEventBus, ari: Ari, scheduleCalendar: ScheduleCalendar, channel: Channel,
  scheduleExecutor: ScheduleActionExecutor)
    extends Actor with LogWithChannel {

  implicit val ec: ExecutionContext = context.dispatcher

  override def preStart(): Unit = {
    chanLog.info(channel, s"$self starting")
  }

  override def postStop(): Unit = {
    chanLog.info(channel, s"$self stop")
  }

  override def receive: Receive = {
    case e: StasisStart =>
      chanLog.info(channel, "entering schedule checks")

      val routePath: Option[RoutePath] = for {
        pathId <- e.args.headOption
        path <- e.args.lastOption
      } yield RoutePath(path, pathId.toInt)

      scheduleCalendar.processSchedule(routePath, e.channel)
        .map(schedulePeriod => scheduleExecutor.processSchedulePeriod(schedulePeriod, e.channel))
        .recover{ case e: Exception => context.parent ! Status.Failure(e) }

    case e: ChannelVarset => chanLog.debug(channel, s"Received ws event $e")

    case ContinueInDialplan(error) => continueInDialplan(error)

    case other => chanLog.warn(channel, s"Unsupported event received $other")
  }

  private def continueInDialplan(msg: String) = {
    ari.channels.setChannelVar(channel.id, XIVO_OUTCALL_ERROR, Some(msg), channel.id)
    ari.channels.continue(channel.id, None, Some("failed"), Some(1), None, channel.id)
  }
}
