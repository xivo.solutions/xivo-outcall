package services.schedule

import org.apache.pekko.actor.SupervisorStrategy.Resume
import org.apache.pekko.actor.{Actor, ActorRef, ActorSystem, OneForOneStrategy, Props, Status}
import app.Configuration.ServiceId
import app.helpers.LogWithChannel
import ari.actions.Ari
import models.Channel
import models.events.ws.{ChannelVarset, StasisEnd, StasisStart}
import services.AriEventBus
import services.AriEventBus._
import services.schedule.ScheduleServiceManager.ContinueInDialplan

import scala.collection.immutable.HashMap
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

object ScheduleServiceManager {
  case class ContinueInDialplan(error: String)
}

class ScheduleServiceManager(ariEventBus: AriEventBus, scheduleCalendar: ScheduleCalendar)(implicit system: ActorSystem, ec: ExecutionContext)
    extends Actor with LogWithChannel {

  var initializedServiceIds: HashMap[ServiceId, ActorRef] = HashMap()

  val ari = new Ari(ariEventBus, ScheduleApp)
  val scheduleActionExecutor = new ScheduleActionExecutor(ari)

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1.minute) {
      case e: Exception =>
        val aRef = sender()
        aRef ! ContinueInDialplan(e.getMessage)
        logger.error(s"Schedule service failed with error. ${e.getMessage}")
        Resume
    }

  override def preStart(): Unit = {
    logger.info(s"$self starting")
    ariEventBus.subscribe(self, AriTopic(topicType = TopicType.WSEVENT, ScheduleApp))
    ariEventBus.subscribe(self, AriTopic(topicType = TopicType.RESTRESPONSE, ScheduleApp))
  }

  override def postStop(): Unit = {
    logger.info(s"$self stop")
    ariEventBus.unsubscribe(self)
  }

  override def receive: Receive = {
    case msg: AriWsMessage =>
      msg.message match {
        case e: StasisStart =>
          val channel = e.channel
          chanLog.debug(channel, s"Received ws event $e")
          val newScheduleService = initializeNewService(e.channel)
          newScheduleService.forward(msg.message)

        case e: ChannelVarset => initializedServiceIds.getOrElse(e.channel.id, initializeNewService(e.channel)).forward(e) //TODO fix remove getOrElse
        case e: StasisEnd =>
          getServiceForChannel(e.channel.id).foreach { ref =>
            context.stop(ref)
            initializedServiceIds -= e.channel.id
          }
        case other => logger.warn(s"Unsupported ws event $other")
      }
    case msg: AriResponseMessage =>
      logger.debug(s"Received rest event $msg")
      getServiceForChannel(msg.serviceId).foreach(_ ! msg.answer)

    case Status.Failure(e) =>
      sender() ! ContinueInDialplan(e.getMessage)
      logger.error(s"Schedule service failed with error. $e")

    case other => logger.error(s"Should not get message here $other")
  }

  private def initializeNewService(channel: Channel): ActorRef = {
    val serviceId = channel.id
    val newScheduleService: ActorRef = context.actorOf(
      Props(new ScheduleService(ariEventBus, ari, scheduleCalendar, channel, scheduleActionExecutor))
    )
    initializedServiceIds += serviceId -> newScheduleService
    newScheduleService
  }

  private def getServiceForChannel(serviceId: ServiceId): Option[ActorRef] = {
    if (initializedServiceIds.contains(serviceId))
      initializedServiceIds.get(serviceId)
    else {
      logger.debug(s"Received response for uninitialized service $serviceId")
      None
    }
  }

}
