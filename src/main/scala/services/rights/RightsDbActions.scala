package services.rights

import anorm.SqlParser.get
import anorm.{ Macro, RowParser, SQL, on, as, ~ }
import app.DatabaseConnectionPool
import services.rights.RightsDbActions._

import scala.concurrent.Future

object RightsDbActions {
  case class LineFeatures(id: Int, name: String, number: String, context: String)
  case class UserLine(id: Int, userId: Int, lineId: Int, extensionId: Int, mainUser: Boolean, mainLine: Boolean)
  case class Extension(id: Int, exten: Int, context: String)
  case class RightCallExten(rightcallid: Int, exten: String)
  case class RightCall(authorization: Int, password: String)
  case class User(id: Int, rightcallcode: Option[String], callrecord: Boolean, outcallerid: String, enablexfer: Boolean,
    onlineCallrecording: Boolean)
  case class GroupFeatures(groupId: Int)
}

class RightsDbActions(db: DatabaseConnectionPool) {

  def getLine(name: String): Future[Option[LineFeatures]] = db.withConnection { implicit c =>
      val simple: RowParser[LineFeatures] = Macro.namedParser[LineFeatures]
      SQL(
        """
           SELECT
                       id, name, number, context
           FROM
                       linefeatures l
           WHERE
                       l.name = {lineName}
        """.stripMargin
      )
        .on(Symbol("lineName") -> name)
        .as(simple.singleOpt)
  }

  def getUser(userId: Int): Future[Option[User]] = db.withConnection { implicit c =>
      val simple: RowParser[User] =
        get[Int]("id") ~
          get[Option[String]]("rightcallcode") ~
          get[Int]("callrecord") ~
          get[String]("outcallerid") ~
          get[Int]("enablexfer") ~
          get[Int]("enableonlinerec") map {
            case id ~ rightcallcode ~ callrecord ~ outcallerid ~ enablexfer ~ enableonlinerec =>
              val callrecordBool = if (callrecord == 1) true else false
              val enablexferBool = if (enablexfer == 1) true else false
              val enableOnlineRecBool = if (enableonlinerec == 1) true else false
              User(id, rightcallcode, callrecordBool, outcallerid, enablexferBool, enableOnlineRecBool)
          }
      SQL(
        """
           SELECT
                       id, rightcallcode, callrecord, outcallerid, enablexfer, enableonlinerec
           FROM
                       userfeatures u
           WHERE
                       u.id = {userId}
        """.stripMargin
      )
        .on(Symbol("userId") -> userId)
        .as(simple.singleOpt)
  }

  def getUserLine(lineId: Int): Future[Option[UserLine]] = db.withConnection { implicit c =>
      val simple: RowParser[UserLine] = Macro.namedParser[UserLine]
      SQL(
        """
           SELECT
                       id, user_id AS userId, line_id AS lineId, extension_id AS extensionId, main_user AS mainUser,
                       main_line AS mainLine
           FROM
                       user_line u
           WHERE
                       u.line_id = {lineId}
        """.stripMargin
      )
        .on(Symbol("lineId") -> lineId)
        .as(simple.singleOpt)
  }

  def getMainExtensionByUserId(userId: String): Future[Option[Extension]] = db.withConnection { implicit c =>
      val simple: RowParser[Extension] = Macro.namedParser[Extension]
      SQL(
        """
           SELECT
                       extensions.id, exten, context
           FROM
                       extensions e
           LEFT JOIN
                       user_line u ON e.id = u.extension_id
           AND
                       u.user_id = {userId}
           AND
                       u.main_line = {mainLine}
           WHERE
                       l.name = {lineName}
        """.stripMargin
      )
        .on(Symbol("userId") -> userId.toInt, Symbol("mainLine") -> true)
        .as(simple.singleOpt)
  }

  def getRightCallExten: Future[List[RightCallExten]] = db.withConnection { implicit c =>
      val simple: RowParser[RightCallExten] = Macro.namedParser[RightCallExten]
      SQL(
        """
           SELECT
                       rightcallid, exten
           FROM
                       rightcallexten r
        """.stripMargin
      )
        .as(simple.*)
  }

  def getRightCallRoute(routeId: String, rightCallsIds: List[RightCallExten]): Future[List[RightCall]] = db.withConnection { implicit c =>
      val simple: RowParser[RightCall] = Macro.namedParser[RightCall]
      SQL(
        """
           SELECT
                       rightcall.authorization AS authorization, rightcall.passwd AS password
           FROM
                       rightcall
           INNER JOIN
                       rightcallmember ON rightcall.id = rightcallmember.rightcallid
           INNER JOIN
                       route ON CAST(rightcallmember.typeval AS integer) = route.id
           WHERE
                       rightcall.id IN ({rightcallids})
           AND
                       rightcallmember.type = 'route'
           AND
                       route.id = {routeid}
           AND
                       rightcall.commented = 0
        """.stripMargin
      )
        .on(Symbol("routeid") -> routeId.toInt, Symbol("rightcallids") -> rightCallsIds.map(r => r.rightcallid))
        .as(simple.*)
  }

  def getRightCallUser(userId: String, rightCallsIds: List[RightCallExten]): Future[List[RightCall]] = db.withConnection { implicit c =>
      val simple: RowParser[RightCall] = Macro.namedParser[RightCall]
      SQL(
        """
           SELECT
                       rightcall.authorization AS authorization, rightcall.passwd AS password
           FROM
                       rightcall
           INNER JOIN
                       rightcallmember ON rightcall.id = rightcallmember.rightcallid
           WHERE
                       rightcall.id IN ({rightcallids})
           AND
                       rightcallmember.type = 'user'
           AND
                       rightcallmember.typeval = {userId}
           AND
                       rightcall.commented = 0
        """.stripMargin
      )
        .on(Symbol("userId") -> userId, Symbol("rightcallids") -> rightCallsIds.map(r => r.rightcallid))
        .as(simple.*)
  }

  def getGroupFeatures(userId: Int): Future[List[GroupFeatures]] = db.withConnection { implicit c =>
      val simple: RowParser[GroupFeatures] = Macro.namedParser[GroupFeatures]
      SQL(
        """
           SELECT
                       groupfeatures.id as groupId
           FROM
                       groupfeatures
           INNER JOIN
                       queuemember ON groupfeatures.name = queuemember.queue_name
           INNER JOIN
                       queue ON queue.name = queuemember.queue_name
           WHERE
                       groupfeatures.deleted = 0
           AND
                       queuemember.userid = {userId}
           AND
                       queuemember.usertype = 'user'
           AND
                       queuemember.category = 'group'
           AND
                       queuemember.commented = 0
           AND
                       queue.category = 'group'
           AND
                       queue.commented = 0
        """.stripMargin
      )
        .on(Symbol("userId") -> userId)
        .as(simple.*)
  }

  def getGroupRights(groupIds: List[Int], rightCallIds: List[RightCallExten]): Future[List[RightCall]] = db.withConnection { implicit c =>
      val simple: RowParser[RightCall] = Macro.namedParser[RightCall]
      SQL(
        """
           SELECT
                       rightcall.authorization AS authorization, rightcall.passwd AS password
           FROM
                       rightcall
           INNER JOIN
                       rightcallmember ON rightcall.id = rightcallmember.rightcallid
           WHERE
                       rightcall.id IN ({rightCallIds})
           AND
                       rightcallmember.type = 'group'
           AND
                       rightcallmember.typeval IN ({groupIds})
           AND
                       rightcall.commented = 0
        """.stripMargin
      )
        .on(Symbol("groupIds") -> groupIds.map(g => g.toString), Symbol("rightCallIds") -> rightCallIds.map(r => r.rightcallid))
        .as(simple.*)
  }
}
