package services.rights

import models.ChannelVariables._
import models._
import services.rights.RightsDbActions.RightCallExten

import scala.concurrent.{ExecutionContext, Future}

class RightsHelper(rightsDbActions: RightsDbActions)(implicit val ec: ExecutionContext) {

  final private val allowedProtocols = List("sip", "sccp", "local", "dahdi", "iax2")

  def isCallForwarded(channelVars: Map[String, Option[String]]): Option[ForwardType] = {
    if (channelVars.get(CALLFORWARDED).flatten.isDefined) Some(ForwardXivo)
    else if (channelVars.get(FORWARDERNAME).flatten.isDefined) Some(ForwardSip)
    else None
  }

  def getDstNumForRoute(channelVars: Map[String, Option[String]], forwardType: Option[ForwardType]): Future[Option[String]] = {
    val routeDestination: Option[String] = channelVars.get(OUTCALL_ID).flatten.flatMap(_ => channelVars.get(DESTINATION_NUMBER).flatten)
    routeDestination match {
      case Some(dst) => Future.successful(Some(dst))
      case None => getDestinationNumber(channelVars, forwardType)
    }
  }

  private def getDestinationNumber(channelVars: Map[String, Option[String]], fwdType: Option[ForwardType]): Future[Option[String]] = {
    fwdType match {
      case Some(fType) => fType match {
        case ForwardXivo =>
          channelVars.get(XIVO_FWD_ACTION).flatten.map {
            case "extension" => Future.successful(channelVars.get(XIVO_FWD_ACTIONARG1).flatten)
            case "user" =>
              channelVars.get(XIVO_FWD_ACTIONARG1).flatten match {
                case Some(fArg) =>
                  for {
                    mainExten <- rightsDbActions.getMainExtensionByUserId(fArg)
                  } yield mainExten.map(_.exten.toString)
                case None => Future.successful(None)
              }
          }.getOrElse(Future.successful(channelVars.get(DESTINATION_NUMBER).flatten))
        case ForwardSip => Future.successful(channelVars.get(DESTINATION_NUMBER).flatten)
      }
      case None => Future.successful(channelVars.get(DESTINATION_NUMBER).flatten)
    }
  }

  private def getUserLine(futureLine: Option[RightsDbActions.LineFeatures]) = {
    futureLine match {
      case Some(l) => rightsDbActions.getUserLine(l.id)
      case None => Future.successful(None)
    }
  }

  def getIdFromChannel(forwarder: String): Future[Forwarded] = {
    val (protocol, lineInterface): (Option[String], Option[String]) = forwarder.split("/") match {
      case Array(proto, interface, _*) => (Some(proto.toLowerCase), Some(interface.split("-")(0)))
      case _ => (None, None)
    }

    if (protocol.isDefined && lineInterface.isDefined && allowedProtocols.contains(protocol.get)) {
      for {
        futureLine <- rightsDbActions.getLine(lineInterface.get)
        futureUserLine <- getUserLine(futureLine)
      } yield futureUserLine.map(_.userId.toString).map(ForwardedUser.apply).getOrElse(ForwardedElseWhere)
    } else {
      Future.successful(ForwardedElseWhere)
    }
  }

  def getIdsForDestination(rightCallsIds: List[RightCallExten], destinationNumber: Option[String]): List[RightCallExten] = {
    destinationNumber match {
      case Some(dst) => rightCallsIds.filter(r => r.exten == dst)
      case None => List()
    }
  }
}
