package services.rights

import org.apache.pekko.actor.{ ActorSystem, Props }
import ari.actions.Ari
import models.Channel
import models.events.ws.{ ChannelVarset, StasisEnd, StasisStart }
import services.AriEventBus._
import services.{ AriEventBus, DatabaseManagers, ServiceManagerFactory }

class RightsFsmManager(ariEventBus: AriEventBus, rightsHelper: RightsHelper, rightsDbActions: RightsDbActions, databaseManagers: DatabaseManagers)(implicit system: ActorSystem) extends ServiceManagerFactory {

  val rightsRepository = new RightsFsmRepository
  val ari = new Ari(ariEventBus, RightsApp)

  override val appType: OutcallApp = RightsApp
  override val ariBus: AriEventBus = ariEventBus
  override def props(channel: Channel): Props = Props(new RightsFsm(ariEventBus, ari, databaseManagers, rightsHelper, rightsRepository, channel))

  override def handleAriWsMessage(msg: AriWsMessage): Unit = {
    logger.debug(s"Received ws event ${msg.message}")
    msg.message match {
      case e: StasisStart =>
        initializeNewFsm(e.channel).forward(msg.message)
      case e: ChannelVarset =>
        initializedFsmIds.getOrElse(e.channel.id, initializeNewFsm(e.channel)).forward(e)
      case e: StasisEnd =>
        getFsmForChannel(e.channel.id).foreach { ref =>
          context.stop(ref)
          initializedFsmIds -= e.channel.id
        }
      case other => logger.warn(s"Unsupported ws event $other")
    }
  }
}
