package services.rights

import models.ChannelVariables._

class RightsFsmRepository {

  def getRequiredChannelVars: List[String] = List(
    XIVO_ROUTE_ID, DESTINATION_NUMBER, CALLFORWARDED, FORWARDERNAME, FWD_REFERER, XIVO_FWD_ACTION, XIVO_FWD_ACTIONARG1
  )
}
