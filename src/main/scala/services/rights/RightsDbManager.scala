package services.rights

import org.apache.pekko.actor.{Actor, ActorSystem, Props}
import org.apache.pekko.pattern.pipe
import app.helpers.LogWithChannel
import models.ChannelVariables._
import models._
import services.ExtPatternMatching
import services.rights.RightsDbActions.{RightCall, RightCallExten, User}
import services.rights.RightsDbManager._
import services.rights.RightsFsm.RichRightsFsmContext

import scala.concurrent.{ExecutionContext, Future}

object RightsDbManager {
  case class ResponseUser(user: Option[User])
  case class ResponseRightCalls(rights: CallRights)

  def props(rightsDbActions: RightsDbActions, rightsHelper: RightsHelper)(implicit system: ActorSystem) = {
    Props(new RightsDbManager(rightsDbActions, rightsHelper))
  }
}

class RightsDbManager(rightDbActions: RightsDbActions, rightsHelper: RightsHelper) extends Actor with LogWithChannel {

  implicit val ec: ExecutionContext = context.dispatcher

  override def preStart(): Unit = {
    logger.info(s"$self starting")
  }

  override def postStop(): Unit = {
    logger.info(s"$self stop")
  }

  private def toResponse(rights: List[RightCall]): ResponseRightCalls = {
    if (rights.nonEmpty)
      ResponseRightCalls(RightsFound(rights))
    else
      ResponseRightCalls(RightsNotFound)
  }

  private def processResponse(actionName: String, actionResult: Future[List[RightCall]]): Future[ResponseRightCalls] = {
    actionResult.map(toResponse)
      .recover {
        case f =>
          logger.error(s"Failed to get $actionName rights. Using empty rights.")
          ResponseRightCalls(RightsFailed(f))
      }
  }

  def receive = {
    case msg: GetRightsIds => getRichRightCall(msg.callId).pipeTo(sender())
    case msg: GetUser => rightDbActions.getUser(msg.id).map(res => ResponseUser(res)).pipeTo(sender())

    case msg: GetRightCallUser =>
      val action = "user"
      processResponse(action, rightDbActions.getRightCallUser(msg.id, msg.extenIds))
        .pipeTo(sender())
    case msg: GetGroupRights =>
      val action = "group"
      processResponse(action, getGroupRules(msg.userId, msg.rightCallIds))
        .pipeTo(sender())
    case msg: GetRightCallRoute =>
      val action = "route"
      processResponse(action, rightDbActions.getRightCallRoute(msg.routeId, msg.rightCallsIds))
        .pipeTo(sender())

    case other =>
      logger.error(s"received unknown message $other")
      throw new Exception(s"received unknown message $other")
  }

  private def getRichRightCall(userRightsContext: RightsFsm.RightsFsmContextWitUserId): Future[RichRightsFsmContext] = {
    def getIdsForDestination(rightCallsIds: List[RightCallExten], destinationNumber: Option[String]) = {
      destinationNumber match {
        case Some(dst) => rightCallsIds.filter(r => ExtPatternMatching.isMatch(r.exten, dst))
        case None => List()
      }
    }

    val userId: Option[Int] = userRightsContext.id
      .orElse(userRightsContext.contextData.getChannelVar(USERID))
      .map(_.toInt)

    def _getUserOpt(maybeInt: Option[Int]): Future[Option[User]] = {
      userId match {
        case Some(id) =>
          rightDbActions.getUser(id)
        case None =>
          Future.successful(None)
      }
    }

    for {
      rightCallsIds <- rightDbActions.getRightCallExten
      destinationNumber <- rightsHelper.getDstNumForRoute(userRightsContext.contextData.vars, userRightsContext.forwardType)
      maybeUser <- _getUserOpt(userId)
    } yield {
      val rightCallsIdsForDestination: List[RightCallExten] = getIdsForDestination(rightCallsIds, destinationNumber)
      RichRightsFsmContext(rightCallsIds, destinationNumber, rightCallsIdsForDestination, maybeUser, List(), userRightsContext.contextData, userRightsContext.channel)
    }
  }

  private def getGroupRules(userId: Int, rightCallIdsSet: List[RightCallExten]): Future[List[RightCall]] = {
    val groupFeaturesFuture: Future[List[RightsDbActions.GroupFeatures]] = rightDbActions.getGroupFeatures(userId)
    groupFeaturesFuture.map { groupFeatures =>
      if (groupFeatures.nonEmpty) {
        rightDbActions.getGroupRights(groupFeatures.map(g => g.groupId), rightCallIdsSet)
      } else {
        Future.successful(List())
      }
    }
      .flatten
  }

}
