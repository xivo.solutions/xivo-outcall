package services.initChannelVars

import org.apache.pekko.actor.{ ActorSystem, Props }
import ari.actions.Ari
import models.Channel
import models.events.ws.{ ChannelVarset, StasisEnd, StasisStart }
import services.AriEventBus._
import services.rights.{ RightsDbActions, RightsHelper }
import services.routing.{ RoutingDbActions, RoutingHelper }
import services.{ AriEventBus, DatabaseManagers, ServiceManagerFactory }

class InitChannelFsmManager(ariEventBus: AriEventBus, userActions: RightsDbActions, userHelper: RightsHelper,
  routeActions: RoutingDbActions, routeHelper: RoutingHelper, initChannelHelper: InitChannelVarsHelper,
  databaseManagers: DatabaseManagers)(implicit system: ActorSystem)
    extends ServiceManagerFactory {

  val ari = new Ari(ariEventBus, InitChannelVarsApp)

  override val appType: OutcallApp = InitChannelVarsApp
  override val ariBus: AriEventBus = ariEventBus

  override def props(channel: Channel): Props = InitChannelVarsFsm.props(ariBus, ari, databaseManagers, routeHelper, userHelper, initChannelHelper, channel)

  override def handleAriWsMessage(msg: AriWsMessage): Unit = {
    logger.debug(s"Received ws event ${msg.message}")
    msg.message match {
      case e: StasisStart =>
        initializeNewFsm(e.channel).forward(msg.message)
      case e: ChannelVarset =>
        initializedFsmIds.getOrElse(e.channel.id, initializeNewFsm(e.channel)).forward(e)
      case e: StasisEnd =>
        getFsmForChannel(e.channel.id).foreach { ref =>
          context.stop(ref)
          initializedFsmIds -= e.channel.id
        }
      case other => logger.warn(s"Unsupported ws event $other")
    }
  }
}
