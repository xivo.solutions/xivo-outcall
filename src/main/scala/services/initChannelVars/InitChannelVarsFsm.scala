package services.initChannelVars

import java.time.Instant

import org.apache.pekko.actor.{ ActorSystem, Props }
import ari.actions.Ari
import models.ChannelVariables._
import models._
import models.events.ws.{ ChannelVarset, StasisStart }
import services.FsmFactory.{ Initial, Uninitialized }
import services._
import services.initChannelVars.InitChannelVarsDbManager.ResponseForwardedUserId
import services.initChannelVars.InitChannelVarsFsm._
import services.rights.RightsDbManager.ResponseUser
import services.rights.RightsHelper
import services.routing.RoutingDbManager.ResponseRoute
import services.routing.RoutingHelper

import scala.concurrent.duration._

object InitChannelVarsFsm {
  case object AwaitChannelVars extends FsmState
  case object AwaitRoute extends FsmState
  case object AwaitSetRouteInDialplan extends FsmState
  case object AwaitUser extends FsmState
  case object AwaitForwardedUserId extends FsmState
  case object AwaitSetCallSettings extends FsmState

  case class InitFsmContext(awaiting: List[String], channel: Channel, channelData: ChannelData,
    routeCallerId: Option[String] = None, internallCallerId: Boolean = false) extends Context

  sealed trait ChannelVariablesSet {
    val name: String
  }

  case class SetUserRecordingFilename(name: String) extends ChannelVariablesSet
  case class SetRouteId(name: String) extends ChannelVariablesSet
  case class SetRouteSubroutine(name: String) extends ChannelVariablesSet
  case class SetXdsUserId(name: String) extends ChannelVariablesSet

  def props(ariEventBus: AriEventBus, ariImp: Ari, databaseManagers: DatabaseManagers,
    routingHelper: RoutingHelper, rightsHelper: RightsHelper,
    initChannelHelper: InitChannelVarsHelper, channelImpl: Channel)(implicit system: ActorSystem): Props = {
    Props(new InitChannelVarsFsm(ariEventBus, ariImp, databaseManagers, routingHelper, rightsHelper, initChannelHelper, channelImpl))
  }
}

class InitChannelVarsFsm(ariEventBus: AriEventBus, ariImpl: Ari, databaseManagers: DatabaseManagers,
    routingHelper: RoutingHelper, rightsHelper: RightsHelper, initChannelHelper: InitChannelVarsHelper,
    channelImpl: Channel) extends FsmFactory {

  val mdsName: String = sys.env.getOrElse(MDS_NAME, "default")
  val requiredChannelVars = List(DESTINATION_NUMBER, XIVO_BASE_CONTEXT, USERID, CALLFORWARDED, FORWARDERNAME,
    FWD_REFERER, XIVO_FWD_ACTION, XIVO_FWD_ACTIONARG1, SOURCE_NUMBER)

  override val ari: Ari = ariImpl
  override val channel: Channel = channelImpl
  override val dbManagers: DatabaseManagers = databaseManagers

  startWith(Initial, Uninitialized)

  when(Initial) {
    case Event(e: StasisStart, Uninitialized) =>
      val channelData = ChannelData(Map()).addChannelVar(MDS_NAME, Some(mdsName))
      requiredChannelVars.foreach(v => ari.channels.getChannelVar(e.channel.id, v, channel.id))
      chanLog.info(e.channel, s"entering init channel variables")
      goto(AwaitChannelVars) using InitFsmContext(requiredChannelVars, e.channel, channelData)
  }

  when(AwaitChannelVars, stateTimeout = 3.seconds) {
    case Event(e: ChannelVarResponse, stateContext: InitFsmContext) =>
      val newCtx = stateContext.copy(
        awaiting = stateContext.channelData.removeOneAwaited(e.channelVarName, stateContext.awaiting),
        channelData = stateContext.channelData.addChannelVar(e.channelVarName, e.channelVarValue.`value`)
      )
      newCtx.waitOrGotoState(AwaitRoute, GetRoute(newCtx.channelData))
  }

  when(AwaitRoute, stateTimeout = 3.seconds) {
    case Event(t: ResponseRoute, stateContext: InitFsmContext) =>
      val firstRoute = t.routes.headOption
      firstRoute match {
        case Some(route) =>
          chanLog.info(channel, s"found available route (routeid:${route.id}) with subroutine ${route.subroutine} and callerId: ${route.callerid}")
          val setChannelVars = initChannelHelper.setRouteId(channel, route)
          val newContext = stateContext.copy(awaiting = setChannelVars.map(_.name), routeCallerId = route.callerid, internallCallerId = route.internalCallerId)
          goto(AwaitSetRouteInDialplan) using newContext
        case None =>
          errorHandling("no available route found")
      }
  }

  when(AwaitSetRouteInDialplan, stateTimeout = 3.seconds) {
    case Event(e: ChannelVarset, stateContext: InitFsmContext) =>
      val newCtx = stateContext.copy(awaiting = stateContext.channelData.removeOneAwaited(e.`variable`, stateContext.awaiting))

      rightsHelper.isCallForwarded(stateContext.channelData.vars) match {
        case Some(forwardType) =>
          newCtx.waitOrGotoState(AwaitForwardedUserId, GetForwardedUserId(getForwarderUserIdIfExists(forwardType, stateContext, rightsHelper.getIdFromChannel)))
        case None =>
          newCtx.channelData.getChannelVar(USERID) match {
            case Some(userId) =>
              newCtx.waitOrGotoState(AwaitUser, GetUser(userId.toInt))
            case None =>
              chanLog.error(channel, "Unable to find user id")
              newCtx.awaitAndStopWithExecute(initChannelHelper.setCallerId(channel, None, stateContext.routeCallerId, stateContext.internallCallerId))
          }
      }
  }

  when(AwaitForwardedUserId, stateTimeout = 3.seconds) {
    case Event(e: ResponseForwardedUserId, stateContext: InitFsmContext) =>
      e.id match {
        case ForwardedUser(id) => stateContext.waitOrGotoState(AwaitUser, GetUser(id.toInt))
        case ForwardedElseWhere => stateContext.awaitAndStop
      }
  }

  when(AwaitUser, stateTimeout = 3.seconds) {
    case Event(e: ResponseUser, stateContext: InitFsmContext) =>
      e.user match {
        case Some(user) =>
          initChannelHelper.setCallerId(channel, Some(user.outcallerid), stateContext.routeCallerId, stateContext.internallCallerId)
          val callOptionsVars = initChannelHelper.setCallOptionVars(channel, stateContext.internallCallerId, user.enablexfer, user.onlineCallrecording)
          val xdsUserIdVars = initChannelHelper.setXdsUserId(channel, user.id.toString)

          val recordingVars = if (user.callrecord) {
            val epochTime = Instant.now.getEpochSecond
            initChannelHelper.setUserRecordingFilename(channel, stateContext.channelData, epochTime)
          } else {
            List()
          }

          val awaiting = (recordingVars ++ xdsUserIdVars).map(_.name) ++ callOptionsVars.map(_.name)
          if (awaiting.length > 0) {
            val newContext = stateContext.copy(awaiting = awaiting)
            goto(AwaitSetCallSettings) using newContext
          } else {
            stateContext.awaitAndStop
          }

        case None => stateContext.awaitAndStop
      }
  }

  when(AwaitSetCallSettings, stateTimeout = 3.seconds) {
    case Event(e: ChannelVarset, stateContext: InitFsmContext) =>
      val newCtx = InitFsmContext(stateContext.channelData.removeOneAwaited(e.`variable`, stateContext.awaiting), stateContext.channel, stateContext.channelData)
      newCtx.awaitAndStop
  }
}
