package services.initChannelVars

import app.helpers.LogWithChannel
import ari.actions.Ari
import models.ChannelVariables._
import models.DialOptions._
import models.{ Channel, ChannelData }
import services.AriEventBus
import services.AriEventBus.InitChannelVarsApp
import services.initChannelVars.InitChannelVarsFsm._
import services.initChannelVars.InitChannelVarsHelper.ChannelVariable
import services.routing.RoutingDbActions.Route

import scala.util.matching.Regex

object InitChannelVarsHelper {
  case class ChannelVariable(name: String, value: String)
}

class InitChannelVarsHelper(ariEventBus: AriEventBus) extends LogWithChannel {
  val ari = new Ari(ariEventBus, InitChannelVarsApp)

  val CALLERID_MATCHER: Regex = """(?:"(.+)"|([a-zA-Z0-9\-\.\!%\*_\+`\'\~]+)) ?(?:<(\+?[0-9\*#]+)>)?$""".r
  val CALLERIDNUM_MATCHER: Regex = """(^\+?[0-9\*#]+$)""".r

  val anonymousVars = List(ChannelVariable("CALLERID(name-pres)", "prohib"), ChannelVariable("CALLERID(num-pres)", "prohib"))

  def setCallerId(channel: Channel, userCallerId: Option[String], routeCallerId: Option[String], routeInternalCallerId: Boolean): List[ChannelVariable] = {
    if (routeInternalCallerId) List() else {
      val vars = getCallerIdVars(userCallerId: Option[String], routeCallerId: Option[String])
      vars.map {
        channelVariable =>
          setChannelVars(channel, channelVariable.name, Some(channelVariable.value))
      }
      vars
    }
  }

  private def getCallerIdVars(userCallerId: Option[String], routeCallerId: Option[String]): List[ChannelVariable] = {
    userCallerId match {
      case Some("default") =>
        routeCallerId.map(getVarsToSet(_)).getOrElse(List())
      case Some("anonymous") =>
        logger.debug(s"Setting anonymous caller id variables")
        routeCallerId.map(getVarsToSet(_)).getOrElse(List()) ++ anonymousVars
      case Some(id) =>
        getVarsToSet(id)
      case None =>
        routeCallerId.map(getVarsToSet(_)).getOrElse(List())
    }
  }

  private def getVarsToSet(callerId: String): List[ChannelVariable] = {
    callerId match {
      case CALLERID_MATCHER(null, stringcase, null) =>
        stringcase match {
          case CALLERIDNUM_MATCHER(num) =>
            logger.info(s"Setting CALLERID(all): $num <$num>")
            List(ChannelVariable("CALLERID(all)", s""""$num <$num>""""))
          case _ =>
            logger.info(s"Unparsable callerId (CALLERIDNUM): $callerId")
            List()
        }
      case CALLERID_MATCHER(calleridname, null, null) =>
        logger.info(s"Setting CALLERID(name): $calleridname")
        List(ChannelVariable("CALLERID(name)", s""""$calleridname""""))

      case CALLERID_MATCHER(calleridname, null, calleridnum) =>
        logger.info(s"Setting CALLERID(all): $calleridname <$calleridnum>")
        List(ChannelVariable("CALLERID(all)", s""""$calleridname <$calleridnum>""""))

      case _ =>
        logger.error(s"Unable to parse callerId (CALLERIDMATCHER): $callerId")
        List()
    }
  }

  def setCallOptionVars(channel: Channel, routeInternalCallerId: Boolean, userEnabledXfer: Boolean, userOnlineCallRec: Boolean): List[ChannelVariable] = {
    val onlineRecording = (routeInternalCallerId, userOnlineCallRec) match {
      case (false, true) => Some(CALLER_STARTREC)
      case _ => None
    }
    val enableXfer = if (userEnabledXfer) Some(CALLER_BLINDXFR) else None
    List(onlineRecording, enableXfer).flatten match {
      case Nil => List()
      case callOptions =>
        val chVars = List(ChannelVariable(XIVO_CALLOPTIONS, callOptions.mkString))
        chVars.foreach { channelVars =>
          setChannelVars(channel, channelVars.name, Some(channelVars.value))
        }
        chVars
    }
  }

  def setChannelVars(channel: Channel, varName: String, varValue: Option[String]): Unit = {
    ari.channels.setChannelVar(channel.id, varName, varValue, channel.id)
  }

  def setUserRecordingFilename(channel: Channel, channelData: ChannelData, epochTime: Long): List[ChannelVariablesSet] = {
    val srcNum = channelData.getChannelVar(SOURCE_NUMBER).getOrElse("")
    val dstNum = channelData.getChannelVar(DESTINATION_NUMBER).getOrElse("")
    val filename = s"user-$srcNum-$dstNum-$epochTime.wav"
    chanLog.info(channel, s"set user's recording filename to $filename")
    setChannelVars(channel, s"$CALL_RECORD_FILE_NAME", Some(filename))
    List(SetUserRecordingFilename(CALL_RECORD_FILE_NAME))
  }

  def setRouteId(channel: Channel, route: Route): List[ChannelVariablesSet] = {
    setChannelVars(channel, s"$XIVO_ROUTE_ID", Some(route.id.toString))
    setChannelVars(channel, s"$OUTCALL_PREPROCESS_SUBROUTINE", route.subroutine)
    List(SetRouteId(XIVO_ROUTE_ID), SetRouteSubroutine(OUTCALL_PREPROCESS_SUBROUTINE))
  }

  def setXdsUserId(channel: Channel, userId: String): List[ChannelVariablesSet] = {
    chanLog.info(channel, s"set xds user id to $userId")
    setChannelVars(channel, s"$XIVO_XDS_USERID", Some(userId))
    List(SetXdsUserId(XIVO_XDS_USERID))
  }
}
