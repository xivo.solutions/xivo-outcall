package services.initChannelVars

import org.apache.pekko.actor.{Actor, ActorRef, ActorSystem, Props}
import app.helpers.LogWithChannel
import models.{Forwarded, GetForwardedUserId}
import services.initChannelVars.InitChannelVarsDbManager.ResponseForwardedUserId
import services.rights.RightsHelper

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

object InitChannelVarsDbManager {
  sealed trait GenericResponse
  case class Response[T](results: List[T]) extends GenericResponse
  case class ResponseForwardedUserId(id: Forwarded) extends GenericResponse

  def props(rightsHelper: RightsHelper)(implicit system: ActorSystem): Props = {
    Props(new InitChannelVarsDbManager(rightsHelper))
  }
}

class InitChannelVarsDbManager(rightsHelper: RightsHelper) extends Actor with LogWithChannel {
  implicit val ec: ExecutionContextExecutor = context.dispatcher

  override def preStart(): Unit = {
    logger.info(s"$self starting")
  }

  override def postStop(): Unit = {
    logger.info(s"$self stop")
  }

  override def receive: Receive = {
    case GetForwardedUserId(processForward) =>
      val requester: ActorRef = sender()
      processForward.onComplete {
        case Success(id) => requester ! ResponseForwardedUserId(id)
        case Failure(e) => logger.error(s"Failed to retrieve user id, $e")
      }

    case other => logger.error(s"Received an unexpected database request $other")
  }
}
