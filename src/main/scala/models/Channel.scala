package models

import play.api.libs.json.{ Json, Reads }

sealed trait DialplanObjects

case class Channel(id: String, name: String, state: String, caller: Caller, connected: Connected, accountcode: Option[String],
    dialplan: Dialplan, creationtime: String, language: String) extends DialplanObjects {
  val resource = "channels"
}

case class Caller(name: Option[String], number: Option[String]) extends DialplanObjects
case class Connected(name: Option[String], number: Option[String]) extends DialplanObjects
case class Dialplan(context: String, exten: String, priority: Int) extends DialplanObjects
case class ChannelVar(`value`: Option[String]) extends DialplanObjects
case class ChannelVarResponse(channelVarName: String, channelVarValue: ChannelVar) extends DialplanObjects
case class NotFound(message: String) extends DialplanObjects

case class ChannelData(vars: Map[String, Option[String]]) {
  def addChannelVar(channelVarName: String, chVarValue: Option[String]): ChannelData =
    ChannelData(vars.+(channelVarName -> chVarValue))
  def getChannelVar(channelVarName: String): Option[String] = {
    val maybeValue = vars.get(channelVarName).flatten
    if (maybeValue.exists(_.trim.nonEmpty)) {
      maybeValue
    } else {
      None
    }
  }
  def removeOneAwaited(channelVarName: String, expectingVars: List[String]): List[String] = expectingVars.filterNot(_ == channelVarName)
}

object DialplanObjects {
  implicit val callerReads: Reads[Caller] = Json.reads[Caller]
}

object Caller {
  implicit val callerReads: Reads[Caller] = Json.reads[Caller]
}

object Connected {
  implicit val connectedReads: Reads[Connected] = Json.reads[Connected]
}

object Dialplan {
  implicit val dialplanReads: Reads[Dialplan] = Json.reads[Dialplan]
}

object Channel {
  type CurrentChannels = List[Channel]
  implicit val channelReads: Reads[Channel] = Json.reads[Channel]
}

object ChannelVar {
  implicit val channelVarReads: Reads[ChannelVar] = Json.reads[ChannelVar]
}

object NotFound {
  implicit val notFoundReads: Reads[NotFound] = Json.reads[NotFound]
}
