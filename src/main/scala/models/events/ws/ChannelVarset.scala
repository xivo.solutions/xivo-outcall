package models.events.ws

import models.Channel
import play.api.libs.json._

case class ChannelVarset(`variable`: String, `value`: String, channel: Channel, asterisk_id: String, application: String) extends WsEvents {
  override val recevingApplication: String = application
}

object ChannelVarset {
  implicit val channelVarsetReads: Reads[ChannelVarset] = Json.reads[ChannelVarset]
}

