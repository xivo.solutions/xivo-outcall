package models.events.bus

case class ChannelVar(name: String, value: Option[String])
