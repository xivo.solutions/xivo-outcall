package models.events.bus

import java.net.URLEncoder

import ari.actions.Channels.HangupReason.HangupReason

sealed trait ChannelActions extends BusEvents {
  def resource = "channels"

  def encode(s: String): String = {
    URLEncoder.encode(s, "UTF-8")
  }
}

case object GetChannels extends ChannelActions
case object GetChannel extends ChannelActions
case class HangupChannel(channelId: String, reason: Option[HangupReason]) extends ChannelActions {
  override def resource: String = s"channels/$channelId?reason=${reason.getOrElse("")}"
}

case class ContinueInDialplan(channelId: String, context: Option[String], extension: Option[String],
    priority: Option[Int], label: Option[String]) extends ChannelActions {
  override def resource: String = s"channels/$channelId/continue?context=${context.getOrElse("")}&extension=${extension.getOrElse("")}" +
    s"&priority=${priority.getOrElse("")}&label=${label.getOrElse("")}"
}

case class SetChannelVar(channelId: String, vars: ChannelVar) extends ChannelActions {
  override def resource: String = s"channels/$channelId/variable?variable=${encode(vars.name)}&value=${encode(vars.value.getOrElse(""))}"
}

case class GetChannelVar(channelId: String, channelVar: String) extends ChannelActions {
  override def resource: String = s"channels/$channelId/variable?variable=$channelVar"
}
