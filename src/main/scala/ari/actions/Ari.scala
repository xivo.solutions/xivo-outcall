package ari.actions

import services.AriEventBus
import services.AriEventBus.OutcallApp

class Ari(ariEventBus: AriEventBus, appName: OutcallApp) {
  val channels: Channels = new Channels(ariEventBus, appName)
}
