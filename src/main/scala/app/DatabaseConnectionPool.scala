package app

import java.sql.Connection
import org.apache.pekko.actor.ActorSystem
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}

class DatabaseConnectionPool(cfg: Configuration)(implicit val system: ActorSystem) {
  implicit val context: ExecutionContextExecutor = system.dispatcher
  val logger: Logger = LoggerFactory.getLogger(getClass)

  def setHikariConfiguration(config: HikariConfig) = {
    config.setDriverClassName("org.postgresql.Driver")
    config.setJdbcUrl("jdbc:postgresql://" + s"${cfg.dbHost}:${cfg.dbPort}" + "/" + "asterisk")
    config.setUsername(cfg.dbUsername)
    config.setPassword(cfg.dbPassword)
    config.setPoolName("xivo-outcall-pool")
    config.setMaximumPoolSize(cfg.dbMaximumPoolSize)
    config.setConnectionTimeout(2000)
  }

  private val config: HikariConfig = new HikariConfig
  setHikariConfiguration(config)

  val dataSource: HikariDataSource = {
    val futureConnection: Future[HikariDataSource] = org.apache.pekko.pattern.Patterns.retry[HikariDataSource](
      () => createDataSource,
      attempts = 60,
      delay = 1.second,
      scheduler = system.scheduler,
      context = system.dispatcher
    )

    try {
      Await.result(futureConnection, cfg.DB_CNX_TIMEOUT)
    } catch {
      case e: Exception =>
        logger.error(s"Unable to connect to database within ${cfg.DB_CNX_TIMEOUT}, exiting the application.")
        system.terminate()
        throw e
    }
  }

  def createDataSource: Future[HikariDataSource] = {
    Future.fromTry(Try(new HikariDataSource(config)))
      .andThen{ case Failure(e) => logger.error("Unable to get connection") }
  }

  def closeConnectionPool: Unit = dataSource.close()

  def withConnection[A](query: Connection => A): Future[A] = {
    val conn = Future(dataSource.getConnection)
    conn.flatMap{c =>
      Future(query(c))
        .andThen { e =>
          e.failed.foreach(_ => logger.error(s"Query statement failed, $e"))
          c.close()
      }
    }
  }
}
