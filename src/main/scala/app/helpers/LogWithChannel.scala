package app.helpers

import com.typesafe.scalalogging.Logger
import models.Channel

trait LogWithChannel {
  val logger: Logger = Logger(getClass.getName)

  private def logMessage(channel: Channel, msg: String): String = s"Channel ${channel.name} (${channel.id}) : $msg"

  object chanLog {
    def debug(channel: Channel, msg: String): Unit = logger.debug(logMessage(channel, msg))
    def info(channel: Channel, msg: String): Unit = logger.info(logMessage(channel, msg))
    def error(channel: Channel, msg: String): Unit = logger.error(logMessage(channel, msg))
    def warn(channel: Channel, msg: String): Unit = logger.warn(logMessage(channel, msg))
  }
}
