package app

import java.time.Clock
import com.typesafe.scalalogging.LazyLogging
import org.apache.pekko.actor.{ActorSystem, Props}
import org.apache.pekko.util.Timeout
import models.{InitChannelVarsFsmManagerService, RightsFsmManagerService, RoutingFsmManagerService}
import org.slf4j.{Logger, LoggerFactory}
import services.initChannelVars.InitChannelVarsHelper
import services.requests.{AriRest, Requester}
import services.rights.{RightsDbActions, RightsHelper}
import services.routing.{RoutingDbActions, RoutingHelper}
import services.schedule.{ScheduleCalendar, ScheduleDbManager, ScheduleServiceManager}
import services.{AriEventBus, DatabaseManagers, ServiceFactoryImpl}
import ws.{ProductionWebsocketRequestFactory, WebsocketAsSink, WebsocketSupervisor}

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration._

object Main extends App with LazyLogging {
  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContextExecutor = system.dispatcher
  implicit val timeout: Timeout = Timeout(10.seconds)
  logger.info("Starting...")

  val config: Configuration = new Configuration
  val dbConnectionPool: DatabaseConnectionPool = new DatabaseConnectionPool(config)

  val ariEventBus: AriEventBus = new AriEventBus
  val ariRest: AriRest = new AriRest
  val scheduleDb = new ScheduleDbManager(dbConnectionPool)
  val scheduleCalendar = new ScheduleCalendar(scheduleDb, initDefaultClock)
  val rightsDb = new RightsDbActions(dbConnectionPool)
  val rightsHelper = new RightsHelper(rightsDb)
  val routingDb = new RoutingDbActions(dbConnectionPool)
  val routingHelper = new RoutingHelper(ariEventBus)
  val initChannelVarsHelper = new InitChannelVarsHelper(ariEventBus)

  val databaseManagers = new DatabaseManagers(rightsDb, rightsHelper, routingDb, routingHelper)

  val serviceFactory = new ServiceFactoryImpl(config, ariEventBus,
    rightsDb, rightsHelper,
    routingDb, routingHelper,
    initChannelVarsHelper, databaseManagers)

  system.actorOf(Props(new Requester(config, ariRest, ariEventBus)), "requester")
  system.actorOf(Props(new ScheduleServiceManager(ariEventBus, scheduleCalendar)), "scheduleManager")

  val productionWebsocketRequestFactory = new ProductionWebsocketRequestFactory(config)
  system.actorOf(WebsocketSupervisor.initWebsocketActor(config, WebsocketAsSink.websocketProps(config, ariEventBus, productionWebsocketRequestFactory)), name = "wsSupervisor")

  system.actorOf(serviceFactory.props(RightsFsmManagerService), "rightsManager")
  system.actorOf(serviceFactory.props(RoutingFsmManagerService), "routingManager")
  system.actorOf(serviceFactory.props(InitChannelVarsFsmManagerService), "initChannelManager")

  scala.sys.addShutdownHook {
    logger.info("Stopping...")
    productionWebsocketRequestFactory.sharedKillSwitch.shutdown()
    system.terminate()
    Await.result(system.whenTerminated, timeout.duration)
    dbConnectionPool.closeConnectionPool
    logger.info("Stopped")
  }

  logger.info("Started")

  def initDefaultClock: Clock = Clock.system(Clock.systemDefaultZone.getZone)
}
