package app

import org.apache.pekko.actor.ActorSystem
import com.typesafe.config.{ Config, ConfigFactory }
import org.slf4j.{ Logger, LoggerFactory }

import scala.concurrent.duration._

object Configuration {
  type ServiceId = String
}

class Configuration(implicit val system: ActorSystem) {
  implicit def asFiniteDuration(d: java.time.Duration): FiniteDuration =
    scala.concurrent.duration.Duration.fromNanos(d.toNanos)

  val log: Logger = LoggerFactory.getLogger(getClass)

  val cfg: Config = ConfigFactory.load()

  val DB_CNX_TIMEOUT = 60.seconds

  def ariHost: String = cfg.getString("ari.xivohost")
  def ariPort: String = cfg.getString("ari.port")
  def ariUsername: String = cfg.getString("ari.username")
  def ariPassword: String = cfg.getString("ari.password")

  def dbHost: String = cfg.getString("db.host")
  def dbPort: String = cfg.getString("db.port")
  def dbUsername: String = cfg.getString("db.username")
  def dbPassword: String = cfg.getString("db.password")
  def dbMaximumPoolSize: Int = cfg.getInt("db.maximumPoolSize")

  def restartRetryMin: FiniteDuration = cfg.getDuration("websocket.restartRetryMin")
  def restartRetryMax: FiniteDuration = cfg.getDuration("websocket.restartRetryMax")

}
