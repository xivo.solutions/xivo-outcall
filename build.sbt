import com.typesafe.sbt.packager.docker.*
import org.clapper.sbt.editsource.EditSourcePlugin
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport.{edit, *}
import sbt.Keys.{javaOptions, libraryDependencies, run, scalacOptions}
import sbt.file
import sbtassembly.AssemblyPlugin.assemblySettings

val appName = "xivo-outcall"
val appVersion = sys.env.get("TARGET_VERSION").getOrElse("2017.03.01")
val appOrganisation = "xivo"

lazy val root = (project in file("."))
  .enablePlugins(DockerPlugin)
  .enablePlugins(JavaServerAppPackaging)
  .settings(
    name := appName,
    version := appVersion,
    scalaVersion := Dependencies.scalaVersion,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-language:implicitConversions"
    )
  )
  .settings(setVersionSettings *)
  .settings(editSourceSettings *)
  .settings(compileSettings *)
  .settings(testSettings *)
  .settings(dockerSettings *)
  .settings(docSettings *)


lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")

lazy val setVersionSettings = Seq(
  setVersionVarTask := { System.setProperty("APPLI_VERSION", appVersion) },
  EditSource / edit := ((EditSource / edit) dependsOn (EditSource / EditSourcePlugin.autoImport.clean)).value
)

assemblySettings

lazy val editSourceSettings = Seq(
  EditSource / flatten := true,
  Universal / mappings += file("conf/appli.version") -> "conf/appli.version",
  EditSource / targetDirectory := baseDirectory.value / "conf",
  EditSource / variables += ("SBT_EDIT_APP_VERSION", appVersion),
  (EditSource / sources) ++= (baseDirectory.value / "src/res" * "appli.version").get,
  EditSource / edit := ((EditSource / edit) dependsOn (EditSource / EditSourcePlugin.autoImport.clean)).value
)

lazy val compileSettings = Seq(
  Compile / packageBin := ((Compile / packageBin) dependsOn (EditSource / edit)).value,
  Compile / run := ((Compile / run) dependsOn setVersionVarTask).evaluated
)

lazy val testSettings = Seq(
  Test / parallelExecution := false,
  Test / javaOptions += "-Dlogger.file=test/resources/logback-test.xml",
  Test / testOptions += Tests.Argument("-oD")
)

lazy val dockerSettings = Seq(
  Docker / maintainer := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "eclipse-temurin:17.0.9_9-jdk-focal",
  dockerExposedVolumes := Seq("/conf","/logs"),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="$appVersion""""),
  dockerEntrypoint := Seq("bin/xivo-outcall-docker")
)


lazy val docSettings = Seq(
  Compile / packageDoc / publishArtifact := false,
  packageDoc / publishArtifact := false,
  Compile / doc / sources := Seq.empty
)

