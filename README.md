# Xivo-outcall

This project is a Asterisk ARI used for making external call.


![scala steward](https://img.shields.io/badge/Scala_Steward-helping-light.svg)
![pipeline](https://gitlab.com/xivo.solutions/xucserver/badges/master/pipeline.svg?ignore_skipped=true)

![scalaversion](https://img.shields.io/badge/Scala%203-DC322F)
![pekkoversion](https://img.shields.io/badge/Pekko-448aff)
![play](https://img.shields.io/badge/Play%20Framework-92d13d)

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
